<div class="mg-b-20 clearfix">
    <div class="float-left">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                @yield('breadcrumb-link')
            </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">@yield('breadcrumb-title')</h4>
    </div>
    <div class="d-none d-md-block float-right">
        @yield('breadcrumb-btn')
    </div>
    <div class="float-right mr-5">@yield('content-header')</div>
</div>
