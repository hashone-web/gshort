@extends('layouts.admin')

@push('content-class', 'content-fixed content-auth-alt')
@push('container-class', 'container ht-100p')

@section('content')
    <div class="ht-100p d-flex flex-column align-items-center justify-content-center">
        <div class="wd-150 wd-sm-250 mg-b-30"><img src="http://themepixels.me/dashforge/assets/img/img17.png" class="img-fluid" alt=""></div>
        <h4 class="tx-20 tx-sm-24">{{ __('Verify Your Email Address') }}</h4>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
        @endif
        <p class="tx-color-03 mg-b-40">
            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }},
        </p>
        <div class="tx-13 tx-lg-14 mg-b-40">
            <form class="d-inline" method="POST" action="">
                @csrf
                <button type="submit" class="btn btn-brand-02 d-inline-flex align-items-center">{{ __('Resend Verification') }}</button>
            </form>
            <a href="" class="btn btn-white d-inline-flex align-items-center mg-l-5">Contact Support</a>
        </div>
    </div>
@endsection
