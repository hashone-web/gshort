@extends('layouts.admin')

@push('content-class', 'content-fixed content-auth-alt')
@push('container-class', 'container d-flex justify-content-center ht-100p')

@section('content')
    <div class="mx-wd-300 wd-sm-450 ht-100p d-flex flex-column align-items-center justify-content-center">
        <div class="wd-80p wd-sm-300 mg-b-15"><img src="http://themepixels.me/dashforge/assets/img/img18.png" class="img-fluid" alt=""></div>
        <h4 class="tx-20 tx-sm-24">{{ __('Reset your password') }}</h4>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <p class="tx-color-03 mg-b-30 tx-center">Enter your username or email address and we will send you a link to reset your password.</p>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="wd-100p d-flex flex-column flex-sm-row mg-b-40">
                <input id="email" type="email" class="form-control wd-sm-250 flex-fill @error('email') is-invalid @enderror" name="email" placeholder="Enter username or email address" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <button class="btn btn-brand-02 mg-sm-l-10 mg-t-10 mg-sm-t-0">{{ __('Reset Password') }}</button>
            </div>
        </form>
    </div>
@endsection
