@extends('layouts.admin')

@php($title = 'Devices')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title . ' (' . $devices->total() . ')')

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('content-header')
    <div class="float-left ml-3">
        <a class="btn btn-primary" href="{{ route('devices.index') }}"><i class="fa fa-redo"></i></a>
    </div>

    <div class="float-left ml-5" style="padding: 9px 0 8px 0;">
        <input type="checkbox" class="custom-control-input" id="is_login" name="is_login" value="1" onchange="filterDevices('{{ route('devices.index', $query_parameters) }}', 'is_login')" @if(isset($query_parameters['is_login']) && $query_parameters['is_login'] == 1) checked @endif>
        <label class="custom-control-label" for="is_login">Login</label>
    </div>

    <div class="float-left ml-5" style="padding: 9px 0 8px 0;">
        <input type="checkbox" class="custom-control-input" id="is_location" name="is_location" value="1" onchange="filterDevices('{{ route('devices.index', $query_parameters) }}', 'is_location')" @if(isset($query_parameters['is_location']) && $query_parameters['is_location'] == 1) checked @endif>
        <label class="custom-control-label" for="is_location">Location</label>
    </div>
    
    <div class="float-left ml-5 form-inline">
        <form method="GET" action="{{ route('devices.index') }}" autocomplete="off">
            <?php 
                $other_query_parameters = $query_parameters;
                unset($other_query_parameters['search_type']);
                unset($other_query_parameters['search_value']);
            ?>
            
            @foreach($other_query_parameters as $other_query_parameter_key => $other_query_parameter_value)
                <input type="hidden" name="{{ $other_query_parameter_key }}" value="{{ $other_query_parameter_value }}">
            @endforeach
            
            <div class="float-left mr-2">
                <select class="form-control filter-sort-select card-header__filter-wrap__search-type" name="search_type" style="width: 140px" required="">
                    <option value="" selected disabled hidden>Select</option>
                    <option value="name" @if(isset($query_parameters['search_type']) && $query_parameters['search_type'] == 'name')selected @endif>Name</option>
                    <option value="mobile_number" @if(isset($query_parameters['search_type']) && $query_parameters['search_type'] == 'mobile_number')selected @endif>Mobile Number</option>
                </select>
            </div>
            <input class="form-control float-left card-header__filter-wrap__search-value mr-2" type="text" name="search_value" value="{{isset($query_parameters['search_value']) ? $query_parameters['search_value'] : ''}}" placeholder="Search">
            <button type="submit" class="btn btn-primary btn-addon btn-rounded float-left card-header__filter-wrap__search-btn">Search</button>
        </form>
    </div>

    <div class="float-left ml-3">
        <select class="filter-by-notification-select" onchange="location = this.value;" style="width: 150px;">
            <option disabled selected value>Notification</option>
                <option value="{{ route('devices.index', array_merge($query_parameters, ['notification_level' => 0])) }}" @if(isset($query_parameters['notification_level']) && $query_parameters['notification_level'] == 0) selected @endif>On</option>
                <option value="{{ route('devices.index', array_merge($query_parameters, ['notification_level' => 1])) }}" @if(isset($query_parameters['notification_level']) && $query_parameters['notification_level'] == 1) selected @endif>Breaking News</option>
                <option value="{{ route('devices.index', array_merge($query_parameters, ['notification_level' => 2])) }}" @if(isset($query_parameters['notification_level']) && $query_parameters['notification_level'] == 2) selected @endif>Off</option>
        </select>
    </div>

    <div class="float-left ml-3">
        <select class="filter-by-platform-select" onchange="location = this.value;" style="width: 100px;">
            <option disabled selected value>Platform</option>
                <option value="{{ route('devices.index', array_merge($query_parameters, ['platform' => 'Android'])) }}" @if(isset($query_parameters['platform']) && $query_parameters['platform'] == 'Android') selected @endif>Android</option>
                <option value="{{ route('devices.index', array_merge($query_parameters, ['platform' => 'iOS'])) }}" @if(isset($query_parameters['platform']) && $query_parameters['platform'] == 'iOS') selected @endif>iOS</option>
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 20%;">Device ID</th>
                            <th>Login</th>
                            <th>Mobile Number</th>
                            <th>Latest Location</th>
                            <th>Location Details</th>
                            <th scope="col">Platform</th>
                            <th scope="col">Notification Status</th>
                            <th scope="col">Shares</th>
                            <th scope="col">Saved News</th>
                            <th scope="col">Reactions</th>
                            <th scope="col">Reports</th>
                            <th scope="col">Location Permission</th>
                            <th scope="col">@sortablelink('created_at', 'Installed On')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($devices as $device)
                        <tr>
                            <td>{{ $device->unique_id }}<span class="d-none">{{ $device->onesignal_player_id }}</span></td>
                            <td>{{ $device->user !== null ? 1: 0 }}</td>
                            <td>
                                <div>{{ $device->user? $device->user->user->mobile_number: ''  }}</div>
                                <div>{{ $device->user? $device->user->user->name: ''  }}</div>
                            </td>
                            <?php
                                $data = [];
                                $location_data = $device->latest_location !== null? $device->latest_location: null;
                                if($location_data) {
                                    try {
                                        $data = json_decode($location_data->location_details, true);
                                    } catch (\Exception $e) {
                                        dd($e);
                                    }
                                }
                            ?>
                            <td>{{ isset($data['city_name'])? $data['city_name']: '' }}{{ isset($data['state_name'])? ', ' . $data['state_name']: '' }}</td>
                            <td>{{ $device->locations_count }} <a href="{{ route('devices.locations', ['id' => $device->id]) }}" target="_blank"><i class="fa fa-link ml-2"></i></a></td>
                            <td>{{ $device->platform }}</td>
                            <td>
                                @if($device->notification_level == 0)
                                    Off
                                @endif
                                @if($device->notification_level == 1)
                                    On
                                @endif
                            </td>
                            <td>{{ $device->shares_count }}</td>
                            <td>{{ $device->bookmarks_count }}</td>
                            <td>{{ $device->reactions_count }}</td>
                            <td>{{ $device->reports_count }}</td>
                            <td>{{ $device->location_permission }}</td>
                            <td>{{ getDateFormat($device->created_at) }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $devices->appends(\Request::except('page'))->render() !!}
    </div>
@endsection