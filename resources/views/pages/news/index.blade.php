@extends('layouts.admin')

@php($title = 'News')
@if($is_news_useful_page)
@php($title = 'Useful Info')
@endif
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('news.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create News</a>
@endsection

@section('content-header')
    <div class="float-left ml-5" style="padding: 9px 0 8px 0;">
        <a class="news-filter-btn news-filter-btn--status {{ isset($query_parameters) && isset($query_parameters['is_pending']) ? 'active': '' }}" href="javascript:;" onclick="filterNews('{{ route('home', $query_parameters) }}','is_pending',{{ isset($query_parameters) && isset($query_parameters['is_pending']) ? 0: 1 }})">Pending <span class="news-filter-btn-count @if($counts['pending_important']) color-red @endif">{{ $counts['pending'] }}</span></a>
    </div>
    <div class="float-left ml-2" style="padding: 9px 0 8px 0;">
        <a class="news-filter-btn news-filter-btn--status {{ isset($query_parameters) && isset($query_parameters['is_draft']) ? 'active': '' }}" href="javascript:;" onclick="filterNews('{{ route('home', $query_parameters) }}','is_draft',{{ isset($query_parameters) && isset($query_parameters['is_draft']) ? 0: 1 }})">Draft <span class="news-filter-btn-count">{{ $counts['draft'] }}</span></a>
    </div>

    <div class="float-left ml-5" style="padding: 9px 0 8px 0;">
        <a class="news-filter-btn {{ isset($query_parameters) && isset($query_parameters['is_notification_scheduled']) ? 'active': '' }}" href="javascript:;" onclick="filterNews('{{ route('home', $query_parameters) }}','is_notification_scheduled',{{ isset($query_parameters) && isset($query_parameters['is_notification_scheduled']) ? 0: 1 }})">Notification <span class="news-filter-btn-count">{{ $counts['notification_scheduled'] }}</span></a>
    </div>
    <div class="float-left" style="padding: 9px 0 8px 0;">
        <a class="news-filter-btn {{ isset($query_parameters) && isset($query_parameters['is_important']) ? 'active': '' }}" href="javascript:;" onclick="filterNews('{{ route('home', $query_parameters) }}','is_important',{{ isset($query_parameters) && isset($query_parameters['is_important']) ? 0: 1 }})">Important <span class="news-filter-btn-count">{{ $counts['important'] }}</span></a>
    </div>
    <div class="float-left" style="padding: 9px 0 8px 0;">
        <a class="news-filter-btn {{ isset($query_parameters) && isset($query_parameters['is_featured']) ? 'active': '' }}" href="javascript:;" onclick="filterNews('{{ route('home', $query_parameters) }}','is_featured',{{ isset($query_parameters) && isset($query_parameters['is_featured']) ? 0: 1 }})">Featured <span class="news-filter-btn-count">{{ $counts['featured'] }}</span></a>
    </div>
    <div class="float-left ml-3">
        <select class="filter-by-category-select" onchange="location = this.value;" style="width: 120px;">
            <option disabled selected value>Category</option>
            @foreach($categories as $category)
                <option value="{{ route('home', array_merge($query_parameters, ['category' => $category->id])) }}" @if(isset($query_parameters['category']) && $query_parameters['category'] == $category->id) selected @endif>{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="float-left ml-5">
        <select class="sort-by-select" onchange="location = this.value;" style="width: 110px;">
            <option disabled selected value>Sort By</option>
            <option value="{{ route('home', array_merge($query_parameters, ['sort' => 'id'])) }}" @if(isset($query_parameters['sort']) && $query_parameters['sort'] == 'id') selected @endif>ID</option>
            <option value="{{ route('home', array_merge($query_parameters, ['sort' => 'datetime'])) }}" @if(isset($query_parameters['sort']) && $query_parameters['sort'] == 'datetime') selected @endif>News Datetime</option>
            <option value="{{ route('home', array_merge($query_parameters, ['sort' => 'created_at'])) }}" @if(isset($query_parameters['sort']) && $query_parameters['sort'] == 'created_at') selected @endif>Created At</option>
            <option value="{{ route('home', array_merge($query_parameters, ['sort' => 'updated_at'])) }}" @if(isset($query_parameters['sort']) && $query_parameters['sort'] == 'updated_at') selected @endif>Updated At</option>
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
   @foreach($news as $news_single)
        <div class="news-slider">
            <div class="container__image">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-3" style="position: relative;">
                        @if(count($news_single->thumbs))
                        <div class="owl-carousel owl-theme">
                            @foreach($news_single->thumbs as $thumb)
                            <div>
                                <a href="javascript:;" target="popup" onclick="window.open('{{ $thumb->parent->parent_full_path }}','popup','width=600,height=600'); return false;">
                                    <img class="news-media-image" src="{{ $thumb->full_path }}">
                                    @if($thumb->parent_is_video)<i class="fa fa-play-circle media-is-video-icon"></i>@endif
                                    @if($thumb->is_sensitive)<i class="fas fa-eye-slash media-is-sensitive"></i>@endif
                                </a>
                            </div>
                            @endforeach
                        </div>
                        @else
                            <div style="display: flex;height: 100%;align-items: center;justify-content: center;text-align: center;">
                                <span class="text-center">
                                    <a href="{{ route('news.edit', ['id' => $news_single->id, '#media-add-tag']) }}" style="display: flex; padding: 64px; border: 1px dotted #0168fa; border-radius: 0.25rem;">
                                        <img src="{{ asset('assets/img/media-add.png') }}" style="max-height: 72px;">
                                    </a>
                                </span>
                            </div>
                        @endif
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-9 container__image-name fs-hind-vadodara">
                        <div class="clearfix">
                            <div class="float-left">
                                <?php
                                    $show_news_single_title = 1;
                                ?>
                                @can('edit news')
                                    @can('edit all news')
                                    @if($show_news_single_title)<h5 style="font-weight: 600;"><a href="{{ route('news.edit', ['id' => $news_single->id]) }}">{{ $news_single->headline }}</a></h5>@endif
                                    <?php
                                        $show_news_single_title = 0;
                                    ?>
                                    @endcan
                                    @can('edit news')
                                        @if(auth()->id() == $news_single->created_by)
                                            @if($show_news_single_title)<h5 style="font-weight: 600;"><a href="{{ route('news.edit', ['id' => $news_single->id]) }}">{{ $news_single->headline }}</a></h5>@endif
                                            <?php
                                                $show_news_single_title = 0;
                                            ?>
                                        @else
                                            @if($show_news_single_title)<h5 style="font-weight: 600;">{{ $news_single->headline }}</h5>@endif
                                            <?php
                                                $show_news_single_title = 0;
                                            ?>
                                        @endif
                                    @endcan
                                @elsecan
                                    @if($show_news_single_title)<h5 style="font-weight: 600;">{{ $news_single->headline }}</h5>@endif
                                    <?php
                                        $show_news_single_title = 0;
                                    ?>
                                @endcan
                            </div>
                            <div class="float-left">
                                <a href="javascript:;" data-toggle="modal" data-target="#myModal-{{ $news_single->id }}" style="line-height: 26px; margin-left: 15px; font-size: 20px;"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        
                        <div class="author-info mt-2">
                            <span class="right-separator"><img class="reaction-img" src="{{ asset('assets/img/avatar.png') }}">{{ $news_single->created_by_user->name }}</span>
                            <span><img class="reaction-img" src="{{ asset('assets/img/edit.png') }}">{{ $news_single->updated_by_user->name }}</span>
                        </div>
                        <div class="mt-2">
                            <span class="{{ getNewsStatusBadge($news_single->status) }}">{{ $news_single->status }}</span>
                            @if($news_single->is_important)<span class="badge badge-danger">Important</span>@endif
                            @if($news_single->is_featured)<span class="badge badge-violet">Featured</span>@endif
                            @if($news_single->is_useful)<span class="badge badge-primary">Useful Info</span>@endif
                            <?php
                                $notification_datetime = \Carbon::createFromTimeStamp(strtotime($news_single->notification_datetime));
                                $is_notification_scheduled = $notification_datetime->floatDiffInSeconds(\Carbon::now(), false) < 0;
                            ?>
                            
                            @if($news_single->notification_status == 'Sent')<span class="badge badge-info">@if($news_single->is_notification_scheduled && $is_notification_scheduled) <i class="fa fa-eye"></i> @endif Notification @if($news_single->notification_type !== null) <span>({{ $news_single->notification_type }})</span> @endif</span>@endif
                            @if($news_single->is_notification_scheduled && $is_notification_scheduled) <span class="small text-info" style="font-weight: bold;">Scheduled: {{ $notification_datetime->diffForHumans() }}</span> @endif
                        </div>
                        <div class="meta mt-2">
                            @if($news_single->news_source_id !== null)<span class="category right-separator">{{ $news_single->source->name }}</span>@endif
                            <span class="category right-separator">{{ $news_single->categories->pluck('name')->implode(', ') }}</span>
                            <span class="date">{{ getDateFormat($news_single->datetime) }}</span>
                        </div>
                        <div class="mt-2"><p>{!! $news_single->article !!}</p></div>
                        <div class="meta mt-2">
                            <span class="right-separator"><img class="reaction-img" src="{{ asset('assets/img/bookmark.png') }}" style="max-height: 18px;">{{ $news_single->bookmarks_count !== null? $news_single->bookmarks_count: 0 }}</span>
                            <span class="right-separator"><img class="reaction-img reaction-img--share" src="{{ asset('assets/img/share.png') }}">{{ $news_single->shares_whatsapp_count !== null? $news_single->shares_whatsapp_count: 0 }}</span>
                            <span class="mr-4"><img class="reaction-img" src="{{ asset('assets/img/eye.png') }}">{{ $news_single->views !== null? $news_single->views: 0 }}</span>
                            
                            <span class="right-separator"><img class="reaction-img" src="{{ asset('assets/img/media-views.png') }}" style="max-height: 18px; vertical-align: sub;">{{ $news_single->media_views !== null? $news_single->media_views: 0 }}</span>
                            <span class="mr-4"><img class="reaction-img" src="{{ asset('assets/img/media-downloads.png') }}" style="max-height: 18px; vertical-align: top;">{{ $news_single->media_downloads !== null? $news_single->media_downloads: 0 }}</span>
                            <?php
                                $notification_data = ['successful' => 0, 'converted' => 0];
                                foreach($news_single->notifications as $notification) {
                                    $notification_data['converted'] = $notification_data['converted'] + $notification->clickes;
                                }
                            ?>
                            <span><img class="reaction-img" src="{{ asset('assets/img/notification.png') }}" style="max-height: 18px; vertical-align: sub;">{{ $notification_data['converted'] }}</span>
                        </div>
                        @can('delete news')
                        <div class="more-wrap">
                            <div class="more">
                                <button class="more-btn">
                                    <span class="more-dot"></span>
                                    <span class="more-dot"></span>
                                    <span class="more-dot"></span>
                                </button>
                                <div class="more-menu">
                                    <div class="more-menu-caret">
                                        <div class="more-menu-caret-outer"></div>
                                        <div class="more-menu-caret-inner"></div>
                                    </div>
                                    <ul class="more-menu-items" tabindex="-1" role="menu" aria-labelledby="more-btn" aria-hidden="true">
                                        <li class="more-menu-item" role="presentation">
                                            <a href="javascript:;" class="more-menu-btn" role="menuitem" onclick="deleteRecords(event, 'News', 'news-{{ $news_single->id }}-delete')">Delete</a>

                                            <form id="news-{{ $news_single->id }}-delete" action="{{ route('news.delete', ['id' => $news_single->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-body">
            <div class="modal fade" id="myModal-{{ $news_single->id }}" role="dialog">
                <div class="modal-dialog">
                    <div class="news-slider">
                        <div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-12 m-b-20">
                                    @if(isset($news_single->thumbs) && count($news_single->thumbs))
                                    <div class="news-media">
                                        <img style="max-width: 100%;" class="news-media-image news__image" src="{{ $news_single->thumbs->first()->full_path }}">
                                    </div>
                                    @endif
                                 </div>
                                 <div class="col-sm-12 col-md-6 col-lg-12 container__image-name mt-2">
                                     <h4 class="modal-title pl-3 pr-3">{{ $news_single->headline }}</h4>
                                     <div class="modal-title mg-b-0 mt-2 pl-3 pr-3">{!! $news_single->article !!}</div>
                                     <div class="meta meta__footer pl-3 pr-3 mb-3">
                                        <span class="time">{{ getDateFormat($news_single->datetime) }}</span>
                                    </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
        </div>
    @endforeach

    <div class="mt-4">
        {{ $news->links() }}
    </div>
@endsection
