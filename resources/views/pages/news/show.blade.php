@extends('layouts.admin')

@php($title = $news->headline)
@push('title', yieldTitle($title))

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
<div class="header-banner" >
  <div class="header-banner__text">
    For the best experience use <a target="_blank" href="#">CityPlus</a> app on your smartphone
  </div>
  <div class="clearfix">
    <a href="https://play.google.com/store/apps/details?id=com.cityplus.local.news.info"><img src= '/assets/img/playstore.png'></a></a>
  </div>
</div>

<div class="card-stack">
  <div class="news-card">
    <div class="row news__card">
      <div class="col-sm-12 col-md-8 col-lg-6">
          @if(isset($news->thumbs) && count($news->thumbs))
            <div class="news-media news-thum">
              <img class="news-media-image news-thum__img" src="{{ $news->thumbs->first()->full_path }}">
          </div>
          @endif
      </div>
      <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
        <div class="news-card-title news-right-box">
          <span class="headline">{{ $news->headline }}</span>
          <div class="news-card-author-time news-card-author-time-in-title">
              <span class="author mr-1">{{ $news->reporters->first()->name }}</span>
              <span style="position: relative;top: -2px;"><i class="fas fa-circle" style="font-size: 4px"></i></span>
              <span class="time ml-1">{{ getDateFormat($news->datetime) }}</span>
          </div>
          <div class="articleBody">{{ $news->article }}</div>
          <div class="news-card-footer news-right-box">
              <!-- <div class="read-more">read more at <a class="source" target="_blank" href="#"></a></div> -->
            </div>
         </div>
      </div>
    </div> 
  </div>
</div>
@endsection