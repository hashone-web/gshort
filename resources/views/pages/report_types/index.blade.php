@extends('layouts.admin')

@php($title = 'Report Types')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('report.types.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Report Type</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Gujarati Name</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($report_types as $report_type)
                            <tr>
                                <th scope="row">{{ $report_type->id }}</th>
                                <td>{{ $report_type->name }}</td>
                                <td>{{ $report_type->gujarati_name }}</td>
                                <td>{{ getDateFormat($report_type->created_at) }}</td>
                                @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                    <td>
                                        @can('edit other cruds')<a href="{{ route('report.types.edit', ['id' => $report_type->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete other cruds')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'Report Type', 'report-type-{{ $report_type->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="report-type-{{ $report_type->id }}-delete" action="{{ route('report.types.delete', ['id' => $report_type->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $report_types->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
