@extends('layouts.admin')

@php($title = 'Permissions')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('permissions.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Permission</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit users') || auth()->user()->can('delete users'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                        <tr>
                            <th scope="row">{{ $permission->id }}</th>
                            <td>{{ $permission->name }}</td>
                            <td>{{ getDateFormat($permission->created_at) }}</td>
                            @if(auth()->user()->can('edit users') || auth()->user()->can('delete users'))
                                <td>
                                    @can('edit users')<a href="{{ route('permissions.edit', ['id' => $permission->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $permissions->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
