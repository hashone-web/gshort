@extends('layouts.admin')

@php($title = 'Ads Statistics Reports')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    
@endsection

@section('content-header')
    <div class="float-left ml-3">
        <a class="btn btn-primary" href="{{ route('ads.statistics.reports.index') }}"><i class="fa fa-redo"></i></a>
    </div>
    <div class="float-left ml-3">
        <select class="filter-by-campaign-select" onchange="location = this.value;" style="width: 200px;">
            <option disabled selected value>Campaign</option>
            @foreach($campaigns as $campaign)
                <option value="{{ route('ads.statistics.reports.index', array_merge($query_parameters, ['campaign' => $campaign->id])) }}" @if(isset($query_parameters['campaign']) && $query_parameters['campaign'] == $campaign->id) selected @endif>{{ $campaign->name }}</option>
            @endforeach
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div id="graph"></div>
    </div>
    <div class="card mt-4">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0" id="ads-statistics-reports-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Impressions</th>
                                <th>Clicks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ads_statistics_reports as $date => $report)
                                <tr>
                                    <td>{{ $date }}</td>
                                    <td>{{ adjustAdsData($report['impressions']) }}</td>
                                    <td>{{ $report['clicks'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4">
        
    </div>
@endsection

@section('last-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#ads-statistics-reports-table').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
                "iDisplayLength": 20
            });
        });
    </script>
@endsection