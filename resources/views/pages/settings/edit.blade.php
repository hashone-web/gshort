@extends('layouts.admin')

@php($title = 'Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="contact_email">Contact Email:</label>
                        <input type="email" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : '' }}" id="contact_email" placeholder="jhalakjaviya@gmail.com" name="contact_email" value="{{isset($setting) ? $setting->contact_email : old('contact_email') }}">
                        @if ($errors->has('contact_email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_share_app_link">Android Share App Link:</label>
                        <input type="text" class="form-control{{ $errors->has('android_share_app_link') ? ' is-invalid' : '' }}" id="android_share_app_link" name="android_share_app_link" value="{{isset($setting) ? $setting->android_share_app_link : old('android_share_app_link') }}">
                        @if ($errors->has('android_share_app_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_share_app_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_share_description">Android Share Description:</label>
                        <textarea class="form-control{{ $errors->has('android_share_description') ? ' is-invalid' : '' }}" id="android_share_description" name="android_share_description">{{ isset($setting) ? $setting->android_share_description : old('android_share_description') }}</textarea>
                        @if ($errors->has('android_share_description'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_share_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="news_share_bottom_image">News Share Bottom Image:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('news_share_bottom_image') ? ' is-invalid' : '' }}" id="news_share_bottom_image" name="news_share_bottom_image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                            <label class="custom-file-label" for="news_share_bottom_image">Choose file</label>
                        </div>
                        <div class="news_share_bottom_image" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->news_share_bottom_image)
                            <a class="mt-2 d-block" href="{{ $setting->news_share_bottom_image_full_path }}" target="_blank">
                                <img src="{{ $setting->news_share_bottom_image_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('news_share_bottom_image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('news_share_bottom_image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="app_share_image">App Share Image:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('app_share_image') ? ' is-invalid' : '' }}" id="app_share_image" name="app_share_image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                            <label class="custom-file-label" for="app_share_image">Choose file</label>
                        </div>
                        <div class="app_share_image" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->app_share_image)
                            <a class="mt-2 d-block" href="{{ $setting->app_share_image_full_path }}" target="_blank">
                                <img src="{{ $setting->app_share_image_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('app_share_image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('app_share_image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="privacy_policy_page_link">Privacy Policy Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('privacy_policy_page_link') ? ' is-invalid' : '' }}" id="privacy_policy_page_link" name="privacy_policy_page_link" value="{{isset($setting) ? $setting->privacy_policy_page_link : old('privacy_policy_page_link') }}" required>
                        @if ($errors->has('privacy_policy_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('privacy_policy_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="terms_of_use_page_link">Terms of Use Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('terms_of_use_page_link') ? ' is-invalid' : '' }}" id="terms_of_use_page_link" name="terms_of_use_page_link" value="{{isset($setting) ? $setting->terms_of_use_page_link : old('terms_of_use_page_link') }}" required>
                        @if ($errors->has('terms_of_use_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('terms_of_use_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="instagram_page_link">Instagram Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('instagram_page_link') ? ' is-invalid' : '' }}" id="instagram_page_link" name="instagram_page_link" value="{{isset($setting) ? $setting->instagram_page_link : old('instagram_page_link') }}">
                        @if ($errors->has('instagram_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('instagram_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="form-group col-md-3">
                        <div class="mb-2">Ads Type: <span class="tx-danger">*</span></div>
                        <div class="form-inline">
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio1" name="ads_type" class="custom-control-input setting-type-radio" value="none" @if(isset($setting) && $setting->ads_type == 'none') checked @elseif(!isset($setting)) checked @else @endif>
                                <label class="custom-control-label" for="customRadio1">None</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio2" name="ads_type" class="custom-control-input setting-type-radio" value="admob" @if(isset($setting) && $setting->ads_type == 'admob') checked @endif>
                                <label class="custom-control-label" for="customRadio2">Admob</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio3" name="ads_type" class="custom-control-input setting-type-radio" value="facebook" @if(isset($setting) && $setting->ads_type == 'facebook') checked @endif>
                                <label class="custom-control-label" for="customRadio3">Facebook</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio4" name="ads_type" class="custom-control-input setting-type-radio" value="both" @if(isset($setting) && $setting->ads_type == 'both') checked @endif>
                                <label class="custom-control-label" for="customRadio4">Both</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
