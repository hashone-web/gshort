@extends('layouts.admin')

@php($title = 'Application Installs')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <div class="mt-3"><h4><b>Total Installs: </b>{{ $application_installs_total }}</h4></div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/morris.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div id="graph"></div>
    </div>
    <div class="card mt-4">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                	<table class="table table-striped mg-b-0">
                        <thead>
	                        <tr>
	                            <th>Date</th>
                                <th>Count</th>
                                <th>Android</th>
                                <th>iOS</th>
	                        </tr>
                        </thead>
                        <tbody>
                        @foreach($application_installs as $key => $application_install)
                            <tr>
                                <td>{{  \Carbon::createFromFormat('Y-m-d', $application_install->date)->format('d/m/Y') }}</td>
                                <td>{{  $application_install->count }}</td>
                                <td>{{  $application_install->android_count }}</td>
                                <td>{{  $application_install->ios_count }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4">
        {!! $application_installs->appends(\Request::except('page'))->render() !!}
    </div>
@endsection


@section('scripts')
<script src="{{ asset('js/raphael-min.js') }}"></script>
<script src="{{ asset('js/morris.min.js') }}"></script>

<script type="text/javascript">
// Use Morris.Bar

var chart_data = [];
var data = JSON.parse('{!! json_encode($application_installs->items()) !!}');
for (var i = data.length - 1; i >= 0; i--) {
    chart_data.push({
        x: data[i].date,
        android: data[i].android_count,
        ios: data[i].ios_count,
    });
}

Morris.Bar({
    element: 'graph',
    data: chart_data,
    xkey: 'x',
    ykeys: ['android', 'ios'],
    labels: ['Android', 'iOS'],
    xLabelAngle: 35,
    hideHover: 'auto',
    barColors: ['#B21516', '#1531B2'],
});
</script>
@endsection
