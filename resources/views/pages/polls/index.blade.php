@extends('layouts.admin')

@php($title = 'Polls')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('polls.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Poll</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th class="text-center" scope="col">Result</th>
                            <th class="text-center" scope="col">Result Views</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit polls') || auth()->user()->can('delete polls'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($polls as $poll)
                        <tr>
                            <th scope="row">{{ $poll->id }}</th>
                            <td>{{ $poll->title }}</td>
                            <td class="text-center"><a href="{{ route('polls.show', [ 'id' => $poll->id ]) }}"><i class="fa fa-link"></i></a></td>
                            <td class="text-center">{{ $poll->result_views_count }}</td>
                            <td>{{ getDateFormat($poll->created_at) }}</td>
                            @if(auth()->user()->can('edit polls') || auth()->user()->can('delete polls'))
                                <td>
                                    @can('edit polls')<a href="{{ route('polls.edit', ['id' => $poll->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                    @can('delete polls')
                                        <a href="javascript:;" onclick="deleteRecords(event, 'Poll', 'country-{{ $poll->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                        <form id="country-{{ $poll->id }}-delete" action="{{ route('polls.delete', ['id' => $poll->id]) }}" method="POST" style="display: none;">
                                            @method('delete')
                                            @csrf
                                        </form>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $polls->appends(\Request::except('page'))->render() !!}
    </div>
@endsection