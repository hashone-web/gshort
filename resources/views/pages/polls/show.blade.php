@extends('layouts.admin')

@php($title = 'Poll Statistics')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('polls.index') }}">Polls</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-warning btn-uppercase mg-l-5" href="{{ route('polls.index') }}"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
              <div class="card-header bd-b-0 clearfix">
                  <div class="float-left"><h4 class="lh-5 mg-b-0">{{ $poll->title }}</h4></div><div class="float-right"><h4 class="lh-5 mg-b-0">{{ $poll->votes }}</h4></div>
              </div><!-- card-header -->
              <div class="card-body pd-0">
                  <ul class="list-unstyled mg-b-0">
                    @foreach($poll->all_options as $option)
                    <li class="list-item">
                      <div class="media">
                        <div class="crypto-icon crypto-icon-sm bg-success">
                          {{ $option->id }}
                        </div>
                        <div class="media-body mg-l-15">
                          <p class="tx-medium mg-b-0" style="line-height: 36px;">{{ $option->title }}</p>
                        </div>
                      </div><!-- media -->
                      <div class="text-right">
                        <p class="tx-normal tx-rubik mg-b-0">{{ $option->total_votes }}</p>
                        <p class="mg-b-0 tx-12 tx-rubik tx-success">{{ $option->percentage_votes }}%</p>
                      </div>
                    </li>
                    @endforeach
                  </ul>
              </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
@endsection