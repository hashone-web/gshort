@extends('layouts.admin')

@php($title = 'Public Pages')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('public.pages.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Public Page</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('title')</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($public_pages as $public_page)
                            <tr>
                                <th scope="row">{{ $public_page->id }}</th>
                                <td>{{ $public_page->title }} <a href="{{ route('public.pages.show', [ 'slug' => $public_page->slug ]) }}" target="_blank"><i class="fa fa-link ml-1"></i></a></td>
                                <td>{{ getDateFormat($public_page->created_at) }}</td>
                                @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                    <td>
                                        @can('edit other cruds')<a href="{{ route('public.pages.edit', ['id' => $public_page->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete other cruds')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'Public Page', 'public-page-{{ $public_page->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="public-page-{{ $public_page->id }}-delete" action="{{ route('public.pages.delete', ['id' => $public_page->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $public_pages->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
