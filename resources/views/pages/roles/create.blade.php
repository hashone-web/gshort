@extends('layouts.admin')

@php($title = isset($role) ? 'Edit Role - ' . $role->name: 'Create Role')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('roles.index') }}">Roles</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($role) ? route('roles.update', ['id' => $role->id]) : route('roles.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" pattern="^[A-Za-z. ]+$" maxlength="30" value="{{isset($role) ? $role->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="permissions">Permissions: <span class="tx-danger">*</span></label>
                        <select class="form-control role-permissions-select{{ $errors->has('permissions') ? ' is-invalid' : '' }}" id="permissions" name="permissions[]" required multiple>
                            @foreach($permissions as $permission)
                                <option value="{{ $permission->id }}" @if(isset($role) && in_array($permission->id, $role->permissions->pluck('id')->toArray())) selected @endif>{{ $permission->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('permissions'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('permissions') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('roles.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection