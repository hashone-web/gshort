@extends('layouts.admin')

@php($title = 'Admin Panel Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.panel.settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Shares Logic 1</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_scan_period_1">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_scan_period_1') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_scan_period_1" placeholder="Period(in minutes)" name="is_featured_shares_scan_period_1" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_scan_period_1 : old('is_featured_shares_scan_period_1') }}">
                                @if ($errors->has('is_featured_shares_scan_period_1'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_scan_period_1') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_count_1">Shares Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_count_1') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_count_1" placeholder="Shares Count" name="is_featured_shares_count_1" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_count_1 : old('is_featured_shares_count_1') }}">
                                @if ($errors->has('is_featured_shares_count_1'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_count_1') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Shares Logic 2</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_scan_period_2">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_scan_period_2') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_scan_period_2" placeholder="Period(in minutes)" name="is_featured_shares_scan_period_2" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_scan_period_2 : old('is_featured_shares_scan_period_2') }}">
                                @if ($errors->has('is_featured_shares_scan_period_2'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_scan_period_2') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_count_2">Shares Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_count_2') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_count_2" placeholder="Shares Count" name="is_featured_shares_count_2" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_count_2 : old('is_featured_shares_count_2') }}">
                                @if ($errors->has('is_featured_shares_count_2'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_count_1') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Shares Logic 3</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_scan_period_3">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_scan_period_3') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_scan_period_3" placeholder="Period(in minutes)" name="is_featured_shares_scan_period_3" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_scan_period_3 : old('is_featured_shares_scan_period_3') }}">
                                @if ($errors->has('is_featured_shares_scan_period_3'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_scan_period_3') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_shares_count_3">Shares Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_shares_count_3') ? ' is-invalid' : '' }} only-numeric" id="is_featured_shares_count_3" placeholder="Shares Count" name="is_featured_shares_count_3" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_shares_count_3 : old('is_featured_shares_count_3') }}">
                                @if ($errors->has('is_featured_shares_count_3'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_shares_count_3') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Views Logic 1</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_scan_period_1">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_scan_period_1') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_scan_period_1" placeholder="Period(in minutes)" name="is_featured_views_scan_period_1" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_scan_period_1 : old('is_featured_views_scan_period_1') }}">
                                @if ($errors->has('is_featured_views_scan_period_1'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_scan_period_1') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_count_1">Views Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_count_1') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_count_1" placeholder="Views Count" name="is_featured_views_count_1" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_count_1 : old('is_featured_views_count_1') }}">
                                @if ($errors->has('is_featured_views_count_1'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_count_1') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Views Logic 2</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_scan_period_2">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_scan_period_2') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_scan_period_2" placeholder="Period(in minutes)" name="is_featured_views_scan_period_2" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_scan_period_2 : old('is_featured_views_scan_period_2') }}">
                                @if ($errors->has('is_featured_views_scan_period_2'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_scan_period_2') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_count_2">Views Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_count_2') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_count_2" placeholder="Views Count" name="is_featured_views_count_2" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_count_2 : old('is_featured_views_count_2') }}">
                                @if ($errors->has('is_featured_views_count_2'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_count_1') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 p-2">
                        <div class="form-row p-2 is-featured-logic-box">
                            <div class="form-group col-md-12 mb-2">
                                <h5>Views Logic 3</h5>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_scan_period_3">Period(in minutes):</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_scan_period_3') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_scan_period_3" placeholder="Period(in minutes)" name="is_featured_views_scan_period_3" maxlength="15" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_scan_period_3 : old('is_featured_views_scan_period_3') }}">
                                @if ($errors->has('is_featured_views_scan_period_3'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_scan_period_3') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="is_featured_views_count_3">Views Count:</label>
                                <input type="text" class="form-control{{ $errors->has('is_featured_views_count_3') ? ' is-invalid' : '' }} only-numeric" id="is_featured_views_count_3" placeholder="Views Count" name="is_featured_views_count_3" value="{{isset($admin_panel_settings) ? $admin_panel_settings->is_featured_reactions_count_3 : old('is_featured_views_count_3') }}">
                                @if ($errors->has('is_featured_views_count_3'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('is_featured_views_count_3') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection