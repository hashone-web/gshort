<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Events\NewsPublished;

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

Route::get('/pages/{slug}', 'Other\PublicPageController@show')->name('public.pages.show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', 'User\UserController@profileEdit')->name('profile.edit');
    Route::post('/users/{id}', 'User\UserController@update')->name('users.update');

    Route::post('/users/{id}/fcm-token', 'User\UserController@fcmToken')->name('users.fcm.token.store');

    Route::group(['middleware' => ['permission:view user reports']], function () {
        // ReportTypes
        Route::get('/reports', 'Other\ReportController@index')->name('reports.index');
    });

    // Users CRUD
    Route::group(['middleware' => ['permission:edit settings']], function () {
        Route::get('/settings', 'Other\SettingController@edit')->name('settings.edit');
        Route::post('/settings', 'Other\SettingController@update')->name('settings.update');

        Route::group([
            'prefix' => 'reporters-app',
        ], function() {
            Route::get('/settings', 'Reporter\SettingController@edit')->name('reporters-app.settings.edit');
            Route::post('/settings', 'Reporter\SettingController@update')->name('reporters-app.settings.update');
        
            Route::get('/updates', 'Reporter\UpdateController@edit')->name('reporters-app.updates.edit');
            Route::post('/updates', 'Reporter\UpdateController@update')->name('reporters-app.updates.update');
        });

        Route::get('/admin-panel-settings', 'Other\AdminPanelSettingController@edit')->name('admin.panel.settings.edit');
        Route::post('/admin-panel-settings', 'Other\AdminPanelSettingController@update')->name('admin.panel.settings.update');

        Route::get('/updates', 'Other\UpdateController@edit')->name('updates.edit');
        Route::post('/updates', 'Other\UpdateController@update')->name('updates.update');

        Route::get('/application-installs', 'Other\ApplicationInstallController@index')->name('application.installs.index');
        
        Route::get('/devices', 'Other\DeviceController@index')->name('devices.index');
        Route::get('/devices/{id}/locations', 'Other\DeviceController@locations')->name('devices.locations');
    
        Route::get('/devices/static-feedbacks', 'Feedback\StaticFeedbackController@index')->name('devices.feedbacks.static.index');

        Route::get('/news-statistics-reports', 'Statistics\NewsStatisticsReportController@index')->name('news.statistics.reports.index');
        
        Route::get('/ads-statistics-reports', 'Statistics\AdsStatisticsReportController@index')->name('ads.statistics.reports.index');

        Route::get('/custom-notifications/create', 'Notification\CustomNotificationController@index')->name('custom.notifications.index');
        Route::post('/custom-notifications', 'Notification\CustomNotificationController@store')->name('custom.notifications.store');
    });

    // News CRUD
    Route::group(['middleware' => ['permission:view useful info']], function () {
        Route::get('/news/useful', 'News\NewsController@index')->name('news.useful');
    });
    
    Route::group(['middleware' => ['permission:view news']], function () {
        Route::get('/home', 'News\NewsController@index')->name('home');

        Route::group(['middleware' => ['permission:create news']], function () {
            Route::post('/news', 'News\NewsController@store')->name('news.store');
            Route::get('/news/create', 'News\NewsController@create')->name('news.create');
        });

        Route::group(['middleware' => ['permission:edit news']], function () {
            Route::get('/news/{id}/edit', 'News\NewsController@edit')->name('news.edit');
            Route::post('/news/{id}', 'News\NewsController@update')->name('news.update');
        });

        Route::group(['middleware' => ['permission:delete news']], function () {
            Route::delete('/news/{id}', 'News\NewsController@destroy')->name('news.delete');
            Route::post('/news/media/{id}/delete', 'News\NewsMediaController@destroy')->name('news.media.delete');
        });
    });


    // Users CRUD
    Route::group(['middleware' => ['permission:view users']], function () {
        Route::get('/users', 'User\UserController@index')->name('users.index');

        Route::group(['middleware' => ['permission:create users']], function () {
            Route::post('/users', 'User\UserController@store')->name('users.store');
            Route::get('/users/create', 'User\UserController@create')->name('users.create');
        });

        Route::group(['middleware' => ['permission:edit users']], function () {
            Route::get('/users/{id}/edit', 'User\UserController@edit')->name('users.edit');
        });

        Route::group(['middleware' => ['permission:delete users']], function () {
            Route::delete('/users/{id}', 'User\UserController@destroy')->name('users.delete');
        });
    });

    
    // Roles CRUD
    Route::group(['middleware' => ['permission:edit roles']], function () {
        Route::get('/roles', 'Permission\RoleController@index')->name('roles.index');
        Route::post('/roles', 'Permission\RoleController@store')->name('roles.store');
        Route::get('/roles/create', 'Permission\RoleController@create')->name('roles.create');
        Route::post('/roles/{id}', 'Permission\RoleController@update')->name('roles.update');
        Route::get('/roles/{id}/edit', 'Permission\RoleController@edit')->name('roles.edit');
    });


    // Permissions CRUD
    Route::group(['middleware' => ['permission:edit permissions']], function () {
        Route::get('/permissions', 'Permission\PermissionController@index')->name('permissions.index');
        Route::post('/permissions', 'Permission\PermissionController@store')->name('permissions.store');
        Route::get('/permissions/create', 'Permission\PermissionController@create')->name('permissions.create');
        Route::post('/permissions/{id}', 'Permission\PermissionController@update')->name('permissions.update');
        Route::get('/permissions/{id}/edit', 'Permission\PermissionController@edit')->name('permissions.edit');
    });


    // Polls CRUD
    Route::group(['middleware' => ['permission:view polls']], function () {
        Route::get('/polls', 'Poll\PollController@index')->name('polls.index');
        
        Route::group(['middleware' => ['permission:create polls']], function () {
            Route::post('/polls', 'Poll\PollController@store')->name('polls.store');
            Route::get('/polls/create', 'Poll\PollController@create')->name('polls.create');
        });

        Route::group(['middleware' => ['permission:edit polls']], function () {
            Route::get('/polls/{id}/edit', 'Poll\PollController@edit')->name('polls.edit');
            Route::post('/polls/{id}', 'Poll\PollController@update')->name('polls.update');
        });

        Route::get('/polls/{id}', 'Poll\PollController@show')->name('polls.show');

        Route::group(['middleware' => ['permission:delete polls']], function () {
            Route::post('/polls/{id}/image/delete', 'Poll\PollController@destroyPollImage')->name('polls.image.delete');
            Route::delete('/polls/{id}', 'Poll\PollController@destroy')->name('polls.delete');
        });
    });


    // Ads CRUD
    Route::group(['middleware' => ['permission:advertisements crud']], function () {
        // Advertisers CRUD
        Route::get('/advertisers', 'Ads\AdvertiserController@index')->name('advertisers.index');
        Route::post('/advertisers', 'Ads\AdvertiserController@store')->name('advertisers.store');
        Route::get('/advertisers/create', 'Ads\AdvertiserController@create')->name('advertisers.create');
        Route::post('/advertisers/{id}', 'Ads\AdvertiserController@update')->name('advertisers.update');
        Route::get('/advertisers/{id}/edit', 'Ads\AdvertiserController@edit')->name('advertisers.edit');
        Route::delete('/advertisers/{id}', 'Ads\AdvertiserController@destroy')->name('advertisers.delete');
        

        // Campaigns CRUD
        Route::get('/campaigns', 'Ads\CampaignController@index')->name('campaigns.index');
        Route::post('/campaigns', 'Ads\CampaignController@store')->name('campaigns.store');
        Route::get('/campaigns/create', 'Ads\CampaignController@create')->name('campaigns.create');
        Route::post('/campaigns/{id}', 'Ads\CampaignController@update')->name('campaigns.update');
        Route::get('/campaigns/{id}/edit', 'Ads\CampaignController@edit')->name('campaigns.edit');
        Route::delete('/campaigns/{id}', 'Ads\CampaignController@destroy')->name('campaigns.delete');
    

        Route::post('/campaigns/banners/{id}/delete', 'Ads\CampaignBannerController@destroy')->name('campaigns.banners.delete');
    });


    // Other CRUD
    Route::group(['middleware' => ['permission:view other cruds']], function () {
        // Categories
        Route::get('/categories', 'Other\CategoryController@index')->name('categories.index');

        // News Sources
        Route::get('/news-sources', 'Other\NewsSourceController@index')->name('news.sources.index');

        // PublicPages
        Route::get('/public-pages', 'Other\PublicPageController@index')->name('public.pages.index');

        // ReportTypes
        Route::get('/report-types', 'Other\ReportTypeController@index')->name('report.types.index');

        Route::group(['middleware' => ['permission:create other cruds']], function () {
            // Categories
            Route::post('/categories', 'Other\CategoryController@store')->name('categories.store');
            Route::get('/categories/create', 'Other\CategoryController@create')->name('categories.create');

            // News Sources
            Route::post('/news-sources', 'Other\NewsSourceController@store')->name('news.sources.store');
            Route::get('/news-sources/create', 'Other\NewsSourceController@create')->name('news.sources.create');

            // PublicPages
            Route::post('/public-pages', 'Other\PublicPageController@store')->name('public.pages.store');
            Route::get('/public-pages/create', 'Other\PublicPageController@create')->name('public.pages.create');

            // ReportTypes
            Route::post('/report-types', 'Other\ReportTypeController@store')->name('report.types.store');
            Route::get('/report-types/create', 'Other\ReportTypeController@create')->name('report.types.create');
        });

        Route::group(['middleware' => ['permission:edit other cruds']], function () {
            // Categories
            Route::get('/categories/sorting', 'Other\CategoryController@sorting')->name('categories.sorting.edit');
            Route::post('/categories/sorting', 'Other\CategoryController@sortingUpdate')->name('categories.sorting.update');
            Route::post('/categories/{id}', 'Other\CategoryController@update')->name('categories.update');
            Route::get('/categories/{id}/edit', 'Other\CategoryController@edit')->name('categories.edit');
            Route::post('/categories/{id}/toggle-status', 'Other\CategoryController@toggleStatus')->name('categories.toggle.status');

            // News Sources
            Route::post('/news-sources/{id}', 'Other\NewsSourceController@update')->name('news.sources.update');
            Route::get('/news-sources/{id}/edit', 'Other\NewsSourceController@edit')->name('news.sources.edit');

            // PublicPages
            Route::post('/public-pages/{id}', 'Other\PublicPageController@update')->name('public.pages.update');
            Route::get('/public-pages/{id}/edit', 'Other\PublicPageController@edit')->name('public.pages.edit');

            // ReportTypes
            Route::post('/report-types/{id}', 'Other\ReportTypeController@update')->name('report.types.update');
            Route::get('/report-types/{id}/edit', 'Other\ReportTypeController@edit')->name('report.types.edit');
        });

        Route::group(['middleware' => ['permission:delete other cruds']], function () {
            // Categories
            Route::delete('/categories/{id}', 'Other\CategoryController@destroy')->name('categories.delete');

            // News Sources
            Route::delete('/news-sources/{id}', 'Other\NewsSourceController@destroy')->name('news.sources.delete');

            // PublicPages
            Route::delete('/public-pages/{id}', 'Other\PublicPageController@destroy')->name('public.pages.delete');

            // ReportTypes
            Route::delete('/report-types/{id}', 'Other\ReportTypeController@destroy')->name('report.types.delete');
        });
    });
});

Route::get('/', 'Other\PublicController@index')->name('public.home.index');
Route::get('/news/{id}', 'Other\PublicController@single')->name('public.home.single');

