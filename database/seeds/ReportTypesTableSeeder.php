<?php

use App\ReportType;
use Illuminate\Database\Seeder;

class ReportTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportType::insert([
            [
                'name' => 'Copyright',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        ReportType::insert([
            [
                'name' => 'Sexual',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        ReportType::insert([
            [
                'name' => 'Wrong Information',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        ReportType::insert([
            [
                'name' => 'Other',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
