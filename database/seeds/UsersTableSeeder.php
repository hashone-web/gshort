<?php

use App\UserCity;
use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'Jhalak Javiya',
                'mobile_number' => '8866553779',
                'password'	=> bcrypt('zncjK9Qax3CMFVtt'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Kapeel Patel',
                'mobile_number' => '8000880004',
                'password' => bcrypt('KzCL3cgU8p78Kvxz'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        $user = User::where('mobile_number', '8866553779')->first();
        $user->assignRole('Super Admin');

        $user = User::where('mobile_number', '8000880004')->first();
        $user->assignRole('Super Admin');
    }
}
