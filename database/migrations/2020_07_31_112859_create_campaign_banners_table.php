<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['feed', 'detail'])->default('feed');
            $table->string('media')->nullable();
            $table->enum('media_type', ['image', 'video'])->default('image');
            $table->enum('action', ['call', 'whatsapp', 'link'])->nullable();
            $table->string('action_data')->nullable();
            $table->string('action_image')->nullable();
            $table->string('action_text')->nullable();
            $table->unsignedBigInteger('campaign_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('campaign_id')
                ->references('id')->on('campaigns')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_banners');
    }
}
