<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnesignalPlayerIdAndPlatformAndNotificationLevelToDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->text('onesignal_player_id')->nullable();
            $table->enum('platform', ['Android', 'iOS'])->default('Android');
            $table->smallInteger('notification_level')->default(1); // 0 - Off, 1 - All, 2 - Breaking
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn('onesignal_player_id');
            $table->dropColumn('platform');
            $table->dropColumn('notification_level');
        });
    }
}
