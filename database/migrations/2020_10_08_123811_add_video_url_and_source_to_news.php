<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVideoUrlAndSourceToNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->text('source_link')->nullable();
            $table->unsignedBigInteger('news_source_id')->nullable();
            $table->text('video_url')->nullable();

            $table->foreign('news_source_id')
                ->references('id')->on('news_sources')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('source_link');
            $table->dropForeign('news_news_source_id_foreign');
            $table->dropColumn('news_source_id');
            $table->dropColumn('video_url');
        });
    }
}
