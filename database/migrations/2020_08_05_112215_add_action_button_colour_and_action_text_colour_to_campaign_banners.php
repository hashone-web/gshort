<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActionButtonColourAndActionTextColourToCampaignBanners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_banners', function (Blueprint $table) {
            $table->string('action_button_colour')->nullable();
            $table->string('action_text_colour')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_banners', function (Blueprint $table) {
            $table->dropColumn('action_button_colour');
            $table->dropColumn('action_text_colour');
        });
    }
}
