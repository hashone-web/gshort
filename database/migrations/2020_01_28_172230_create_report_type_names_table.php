<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTypeNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_type_names', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('gujarati_name');
            $table->unsignedBigInteger('report_type_id');
            $table->timestamps();

            $table->foreign('report_type_id')
                ->references('id')->on('report_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_type_names');
    }
}
