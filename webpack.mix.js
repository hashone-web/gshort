const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles(['public/css/custom.css'], 'public/css/custom.min.css')
    .styles(['public/css/public.css'], 'public/css/public.min.css')
    .js(['public/js/custom.js'], 'public/js/custom.min.js')
    .version();
