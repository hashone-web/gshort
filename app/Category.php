<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at',
        'status',
        'sort',
    ];

    protected $appends = ['icon_full_path'];

    public function getIconFullPathAttribute($value)
    {
        return asset('/storage/categories/' . $this->id . '/image/' . '/' . $this->image);
    }
}
