<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Device extends Model
{
    use Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unique_id',
    ];

    public function getLatestLocationAttribute()
    {
        return $this->locations()->first();
    }

    public function shares()
    {
        return $this->hasMany('App\NewsShare', 'device_id');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\DeviceBookmark', 'device_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\NewsReaction', 'device_id');
    }

    public function locations()
    {
        return $this->hasMany('App\DeviceLocation', 'device_id');
    }

    public function user()
    {
        return $this->hasOne('App\UserDevice', 'device_id');
    }
}
