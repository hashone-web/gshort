<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceStaticFeedback extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id'
    ];

    protected $casts = [
        'options' => 'array'
    ];

    public function setOptionsAttribute($value)
    {
    	if($value !== null) {
    		$this->attributes['options'] = $value;
    	}
    }
}
