<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    protected $appends = ['news_share_bottom_image_full_path', 'app_share_image_full_path'];

    public function getNewsShareBottomImageFullPathAttribute($value)
    {
        return $this->news_share_bottom_image !== null? asset('/storage/settings/' . $this->news_share_bottom_image): null;
    }

    public function getAppShareImageFullPathAttribute($value)
    {        
        return $this->app_share_image !== null? asset('/storage/settings/' . $this->app_share_image): null;
    }
}
