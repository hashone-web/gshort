<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class News extends Model
{
    use SoftDeletes, Sortable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'source' => 'object',
    ];

    protected $appends = ['video_type'];

    public function getVideoTypeAttribute($value)
    {
        $video_type = 1;

        if($this->video_url !== null) {
            $source_domain_full = getTopLevelDomain($this->video_url);
            $source_domain = explode('.', $source_domain_full);
            if(count($source_domain) > 1) {
                $source_domain = $source_domain[0];
            }
            if($source_domain == 'youtube') $video_type = 0;
        }

        return $video_type;
    }

    public function getMediaViewsAttribute()
    {
        $data = $this->media_statistics()->first();
        return $data !== null? $data->media_views :0;
    }

    public function getMediaDownloadsAttribute()
    {
        $data = $this->media_statistics()->first();
        return $data !== null? $data->media_downloads :0;
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'news_categories', 'news_id', 'category_id');
    }

    public function created_by_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function source()
    {
        return $this->belongsTo('App\NewsSource', 'news_source_id');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\DeviceBookmark', 'news_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\NewsReaction', 'news_id');
    }

    public function views()
    {
        return $this->hasMany('App\NewsView', 'news_id');
    }

    public function likes()
    {
        return $this->hasMany('App\NewsReaction', 'news_id')->where('type', 'Claps');
    }

    public function dislikes()
    {
        return $this->hasMany('App\NewsReaction', 'news_id')->where('type', 'Sad');
    }

    public function sensitivities()
    {
        return $this->hasMany('App\NewsDeviceSensitivity', 'news_id');
    }

    public function shares()
    {
        return $this->hasMany('App\NewsShare', 'news_id')->where('type', 'Whatsapp');
    }

    public function original_media()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', 'original')->orderBy('sort');
    }

    public function media()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', 'original')->orderBy('sort');
    }

    public function media_statistics()
    {
        return $this->hasOne('App\NewsStatistic', 'news_id');
    }

    public function thumbs()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', \Config::get('constants.thumb_size'))->orderBy('sort');
    }

    public function downloads()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', 'original')->orderBy('sort');
    }

    public function notifications()
    {
        return $this->hasMany('App\NewsNotification', 'news_id');
    }

    public function poll()
    {
        return $this->hasOne('App\Poll', 'news_id');
    }
}
