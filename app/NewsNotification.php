<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsNotification extends Model
{
    // automatically handles json_encode, json_decode to php array
	protected $casts = [
	    'onesignal_notification_data' => 'array'
	];
}
