<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsSource extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function domains()
    {
        return $this->hasMany('App\NewsSourceDomain', 'news_source_id');
    }
}
