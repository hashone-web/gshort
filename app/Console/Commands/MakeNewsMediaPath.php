<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NewsMedia;

class MakeNewsMediaPath extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news_media:path';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $news_media = NewsMedia::all();

        foreach ($news_media as $news_media_single) {
            $news_media_single->full_path = $news_media_single->full_path_dynamic;
            $news_media_single->thumb = $news_media_single->thumb_dynamic;
            $news_media_single->update();
            echo $news_media_single->id;
        }
    }
}
