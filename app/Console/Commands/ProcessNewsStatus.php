<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ProcessNewsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:process_status {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process News Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $all_arguments = $this->arguments();
        $all_data = $all_arguments['data'];

        $data = [ 
            'registration_ids' => $all_data['ids'],
            'data' => [
                'notification' => [
                    'title' => $all_data['title'],
                    'body' => $all_data['content'],
                    'icon' => '/favicon-96x96.png',
                    'data' => [
                        'click_action' => $all_data['url'],
                    ]
                ]
            ]
        ];

        $response = $this->sendNotification($data);

        $this->info('Done');
    }

    public function sendNotification($data)
    {
        $notification_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: key=AAAADRN2KVs:APA91bFweXeoweLs3fyZcSV6b5lWJjALMVIx6VwIovK6Q2gEhgrFSpHFbYGAP3t7qVw1TAZcE0DQ3sRS0QeqCJWyGAKWLkHcnqEBgJQJOhKNwU9JY5Y2PifMyyBXAPRq9suQw1HydlBG'
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 300); //timeout in seconds

        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, true);

        return $response;
    }
}
