<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NewsReaction;
use App\NewsStatisticsReport;
use App\Device;

class GenerateDailyStatisticsReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:daily_statistics_report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Daily News Statistics Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today_date = \Carbon::now()->subHours(24)->format('Y-m-d');
        $previous_date = \Carbon::now()->subHours(48)->format('Y-m-d');

        $this->helper("App\\News", 'news', $today_date, $previous_date);
        $this->helper("App\\NewsReaction", 'reactions_claps', $today_date, $previous_date);
        $this->helper("App\\NewsShare", 'shares', $today_date, $previous_date);
        $this->helper("App\\NewsView", 'views', $today_date, $previous_date);
        $this->helper("App\\NewsStatistic", 'media_views', $today_date, $previous_date);
        $this->helper("App\\NewsStatistic", 'media_downloads', $today_date, $previous_date);
        
        $active_devices_in_last_24_hours = Device::where('last_active_at', '>', \Carbon::now()->subHours(24)->format('Y-m-d H:i:s'))->count();
        $news_statistics_report = NewsStatisticsReport::firstOrNew([ 'type' => 'active_devices_in_last_24_hours', 'date' => $today_date ]);
        $news_statistics_report->data = $active_devices_in_last_24_hours;
        $news_statistics_report->total = $active_devices_in_last_24_hours;
        $news_statistics_report->save();

        $this->info('Done');
    }

    public function helper($model, $type, $today_date, $previous_date)
    {
        $count = 0;

        if($type == 'media_views' || $type == 'media_downloads') {
            $count = app($model)::sum($type);
        } else if($type == 'news') {
            $count = app($model)::where('status', 'Published')->count();
        } else {
            $count = app($model)::count();
        }
        
        $news_statistics_report = NewsStatisticsReport::firstOrNew([ 'type' => $type, 'date' => $today_date ]);

        $news_statistics_report->total = $count;

        $yesterday_news_statistics_report = NewsStatisticsReport::where('type', $type)
                                                ->where('date', $previous_date)->first();

        if($yesterday_news_statistics_report) {
            $news_statistics_report->data = $news_statistics_report->total - $yesterday_news_statistics_report->total;
        } else {
            $news_statistics_report->data = $news_statistics_report->total;
        }

        $news_statistics_report->save();                           
    }
}
