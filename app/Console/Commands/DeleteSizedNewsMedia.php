<?php

namespace App\Console\Commands;

use App\NewsMedia;
use Illuminate\Console\Command;

class DeleteSizedNewsMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news_media:delete_sized {i}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $size = $arguments['i'];

        $news_media_all = NewsMedia::with('news')->whereRaw('LOWER(`extension`) NOT LIKE ? ', ['mp4%'])->whereIn('type', ['original', 'thumb']);
        $news_media_all_ids = $news_media_all->get()->pluck('id')->toArray();
        $news_media_all = $news_media_all->get();

        $public_storage_path = 'app/public/';

        foreach ($news_media_all as $news_media) {
            $path = 'news/' . $news_media->news->id . '/media/' . $news_media->dynamic_id;

            if ($news_media->type == 'thumb') {
                $path = $path . '/thumb';
            }
            $app_path = storage_path($public_storage_path . $path);

            $media_path = $app_path . '/' . $size;

            if (file_exists($media_path)) {
                \File::deleteDirectory($media_path, 0777, true);
            }
        }

        NewsMedia::whereIn('parent_id', $news_media_all_ids)->where('type', $size)->delete();

        $this->info('Done');
    }
}
