<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\NewsNotification;

class NewsNotificationOneSignalData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:onesignal_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch OneSignal Notification Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://onesignal.com/api/v1/notifications?app_id=" . env('ONESIGNAL_APP_ID', 'f255072d-fbef-4219-87f8-46511ec39f13') . "&limit=50&offset=0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic ' . env('ONESIGNAL_APP_KEY', 'OTU4NzIzYmItNmQyZi00ZGUxLTk2OTMtYTJkM2Q2MDJmYjUw')
            ),
        ));

        $notifications = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $notifications = json_decode($notifications, true);
        
        foreach ($notifications['notifications'] as $notification) {
            $news_notification = NewsNotification::where('onesignal_notification_id', $notification['id'])->first();
            if($news_notification) {
                $final_data = [];
                $final_data['successful'] = $notification['successful'];
                $final_data['failed'] = $notification['failed'];
                $final_data['converted'] = $notification['converted'];
                $final_data['remaining'] = $notification['remaining'];
                $news_notification->onesignal_notification_data = $final_data;
                $news_notification->update();
            }
        }

        $this->info('Done');
    }
}
