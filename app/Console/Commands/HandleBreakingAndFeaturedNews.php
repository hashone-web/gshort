<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;

class HandleBreakingAndFeaturedNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $breaking_news_remove_time = \Carbon::now()->subHours(12);
        $breaking_news = News::where('is_breaking', 1)->where('is_breaking_datetime', '<', $breaking_news_remove_time)->update(['is_breaking' => 0]);

        $featured_news_remove_time = \Carbon::now()->subHours(10 * 24);
        $featured_news = News::where('is_featured', 1)->where('is_featured_datetime', '<', $featured_news_remove_time)->update(['is_featured' => 0]);

        $this->info('Done');
    }
}
