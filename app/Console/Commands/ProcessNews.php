<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Device;
use App\NewsNotification;

class ProcessNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:process {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process News Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $all_arguments = $this->arguments();
        $all_data = $all_arguments['data'];

        $data = $all_data['data'];
        $included_segments = $all_data['included_segments'];

        $response = $this->onesignalNotification($data, $included_segments);

        $this->info('Done');
    }

    public function onesignalNotification($data, $included_segments) {
        $notification_data = [
            'contents' => [
                'en' => $data['content']
            ],
            'headings' => [
                'en' => $data['title']
            ],
            'android_group_message' => [
                'en' => ''
            ],
            'data' => $data['additional_data'],
            'priority' => 10,
            'android_visibility' => 1,
            'app_id' => env('ONESIGNAL_APP_ID', 'f81c8144-43d5-414d-a812-bc7778cd9c30'),
        ];

        if(isset($data['send_after'])) {
            $notification_data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i:s', ($data['send_after']))->setTimezone('Asia/Kolkata');
        }

        if(isset($data['big_picture'])) {
            $notification_data['big_picture'] = $data['big_picture'];
        }

        $notification_data['included_segments'] = $included_segments;

        if(in_array('Android Test', $included_segments) || in_array('iOS Test', $included_segments)) {
            $this->onesignalNotificationTest($data, $notification_data, $included_segments);
        } else {
            if(isset($data['additional_data'])) {
                $this->onesignalNotificationAndroid($data, $notification_data, $included_segments);
            }

            if(isset($data['additional_data'])) {
                $this->onesignalNotificationiOS($data, $notification_data, $included_segments);
            }
        }
    }

    public function onesignalNotificationAndroid($data, $notification_data, $included_segments)
    {
        $devices = Device::where('platform', 'Android')
                        ->whereNotNull('onesignal_player_id')
                        ->whereRaw('LENGTH(onesignal_player_id) > 10');
                        
        $devices = $devices->where('notification_level', '!=', 0);             

        $onesignal_players_id = $devices->get()->pluck('onesignal_player_id')->toArray();

        $onesignal_players_id = array_unique($onesignal_players_id);
        $onesignal_players_id_chunks = array_chunk($onesignal_players_id, 999);

        if(in_array('All', $notification_data['included_segments'])) {
            unset($notification_data['included_segments']);
        }

        foreach ($onesignal_players_id_chunks as $onesignal_players_id_chunk) {
            if(!isset($notification_data['included_segments'])) {
                $notification_data['include_player_ids'] = $onesignal_players_id_chunk;
            }

            $notification_data['isAndroid'] = true;

            $notification_data_final = json_encode($notification_data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic ' . env('ONESIGNAL_APP_KEY', 'MWYyMjYwMjctM2VmOC00ZTU1LTk1OGQtNWQ4NzQxNmMwMTIz')
            ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_data_final);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 300); //timeout in seconds

            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response, true);

            if(isset($data['id']) && $response && isset($response['id']) && $response['id']) {
                $this->storeNewsNotification($data['id'], $response, 'Android');
            }
        }

        return [
            'status' => true
        ];
    }

    public function onesignalNotificationiOS($data, $notification_data, $included_segments)
    {
        $devices = Device::where('platform', 'iOS')
                        ->whereNotNull('onesignal_player_id')
                        ->whereRaw('LENGTH(onesignal_player_id) > 10');
                        
        $devices = $devices->where('notification_level', '!=', 0);             

        $onesignal_players_id = $devices->get()->pluck('onesignal_player_id')->toArray();

        $onesignal_players_id = array_unique($onesignal_players_id);
        $onesignal_players_id_chunks = array_chunk($onesignal_players_id, 999);

        if(in_array('All', $notification_data['included_segments'])) {
            unset($notification_data['included_segments']);
        }

        $notification_data['headings']['en'] = strip_tags($notification_data['headings']['en']);
        $notification_data['contents']['en'] = $notification_data['headings']['en'];
        unset($notification_data['headings']);

        foreach ($onesignal_players_id_chunks as $onesignal_players_id_chunk) {
            if(!isset($notification_data['included_segments'])) {
                $notification_data['include_player_ids'] = $onesignal_players_id_chunk;
            }

            $notification_data['isIos'] = true;

            $notification_data_final = json_encode($notification_data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic ' . env('ONESIGNAL_APP_KEY', 'MWYyMjYwMjctM2VmOC00ZTU1LTk1OGQtNWQ4NzQxNmMwMTIz')
            ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_data_final);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 300); //timeout in seconds

            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response, true);

            if(isset($data['id']) && $response && isset($response['id']) && $response['id']) {
                $this->storeNewsNotification($data['id'], $response, 'iOS');
            }
        }

        return [
            'status' => true
        ];
    }

    public function onesignalNotificationTest($data, $notification_data, $included_segments)
    {
        if(in_array('iOS Test', $included_segments)) {
            $notification_data['isIos'] = true;
            $notification_data['contents']['en'] = $notification_data['headings']['en'];
            unset($notification_data['headings']);
        }

        if(in_array('Android Test', $included_segments)) {
            $notification_data['isAndroid'] = true;
        }

        $notification_data_final = json_encode($notification_data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . env('ONESIGNAL_APP_KEY', 'MWYyMjYwMjctM2VmOC00ZTU1LTk1OGQtNWQ4NzQxNmMwMTIz')
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_data_final);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 300); //timeout in seconds

        $response = curl_exec($ch);
        curl_close($ch);

        return [
            'status' => true
        ];
    }

    public function storeNewsNotification($news_id, $data, $platform)
    {
        $news_notification = new NewsNotification();
        $news_notification->news_id = $news_id;
        $news_notification->onesignal_notification_id = $data['id'];
        $news_notification->onesignal_notification_recipients = $data['recipients'];
        $news_notification->platform = $platform;
        $news_notification->save();
    }
}
