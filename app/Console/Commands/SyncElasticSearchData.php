<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;

class SyncElasticSearchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:elastic_search_data {--size=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync ElasticSearch Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $size = (int) $this->option('size');

        $latest_news = News::orderByDesc('created_at')->first();
        
        if($latest_news) {
            $y = 0;
            $i = $latest_news->id;

            while($i > 0) {
                $y++;

                if($size !== 0 && $y > $size) {
                    $i = 0;
                }

                $news = News::with(['poll.options', 'media', 'categories', 'source'])
                    ->addSelect('news.*')
                    ->withCount('shares as shares_whatsapp_count');

                $news = $news->find($i);

                if($news) {
                    $news_categories_ids = $news->categories()->pluck('category_id')->toArray();
                    $news_sensitivities_device_ids = $news->sensitivities()->pluck('device_id')->toArray();
                    $news_bookmarks_device_ids = $news->bookmarks()->pluck('device_id')->toArray();

                    $news = $news->toArray();
                    $news['categories_ids'] = $news_categories_ids;
                    $news['sensitivities'] = $news_sensitivities_device_ids;
                    $news['bookmarks'] = $news_bookmarks_device_ids;

                    \Elasticsearch::update([
                        'id' => $news['id'],
                        'index' => 'short_news',
                        'body' => [
                            'doc' => $news,
                            'doc_as_upsert' => true,
                        ]
                    ]);

                    echo $news['id'];
                }

                $i--;
            }
        }

        $this->info('Done');
    }
}
