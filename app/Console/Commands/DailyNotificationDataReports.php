<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NewsStatisticsReport;
use App\News;
use App\NewsNotification;

class DailyNotificationDataReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:daily_notification_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Notification Data Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Start date
        $notification_clicks_total = 0;
        $notification_count_total = 0;
        $date = date("Y-m-d");
        $date = date("Y-m-d", strtotime("-1 day", strtotime($date)));
        $end_date = date("Y-m-d");

        while (strtotime($date) <= strtotime($end_date)) {
            echo $date;
            $today = $date;
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));

            $news_ids = News::whereBetween('created_at', [$today, $date])->get()->pluck('id')->toArray();
            $notification_clicks_count = NewsNotification::whereIn('news_id', $news_ids)->sum('clickes');
            
            $notification_clicks_total = $notification_clicks_total + $notification_clicks_count;
            $type = 'notification_clicks';
            $this->helper($type, $today, $notification_clicks_count, $notification_clicks_total);

            $notification_count = News::whereIn('id', $news_ids)->whereNotNull('notification_datetime')->count();
            $notification_count_total = $notification_count_total + $notification_count;
            $type = 'notification_count';
            $this->helper($type, $today, $notification_count, $notification_count_total);  
        }

        $this->info('done');
    }

    public function helper($type, $date, $count, $total)
    {
        $news_statistics_report = NewsStatisticsReport::firstOrNew([ 'type' => $type, 'date' => $date ]);

        $news_statistics_report->total = $total;
        $news_statistics_report->data = $count;

        $news_statistics_report->save();                           
    }
}
