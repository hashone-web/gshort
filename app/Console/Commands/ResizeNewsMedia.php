<?php

namespace App\Console\Commands;

use App\News;
use App\NewsMedia;
use Illuminate\Console\Command;

class ResizeNewsMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news_media:resize {i}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resize News Media';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $size = $arguments['i'];

        $news_media_all = NewsMedia::with('news')->has('news')->whereRaw('LOWER(`extension`) NOT LIKE ? ', ['mp4%'])->whereIn('type', ['original', 'thumb']);
        $news_media_all_ids = $news_media_all->get()->pluck('id')->toArray();
        $news_media_all = $news_media_all->get();

        NewsMedia::whereIn('parent_id', $news_media_all_ids)->where('type', $size)->delete();

        $public_storage_path = 'app/public/';

        foreach ($news_media_all as $news_media) {
            $path = 'news/' . $news_media->news->id . '/media/' . $news_media->dynamic_id;

            if($news_media->type == 'thumb') {
                $path = $path . '/thumb';
            }
            $app_path = storage_path($public_storage_path . $path);

            $original_media = \Image::make($app_path . '/' . $news_media->name);

            $height = str_replace('px', '', $size);

            if(strpos($size, 'pc') !== false) {
                $height = str_replace('pc', '', $size);
                $height = ($original_media->height() * $height) / 100;
            }

            $media_path = $app_path . '/' . $size;

            if (file_exists($media_path)) {
                \File::deleteDirectory($media_path, 0777, true);
                if (!file_exists($media_path)) {
                    \File::makeDirectory($media_path, 0777, true);
                }
            } else {
                \File::makeDirectory($media_path, 0777, true);
            }

            $resized_media = $original_media
                ->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($media_path . '/' . $news_media->name, 90);

            $this->storeNewsMedia($news_media->news, $news_media, $resized_media, $size);
        }

        $this->info('Done');
    }

    protected function storeNewsMedia($news, $news_media, $resized_media, $size)
    {
        $news_resized_media = new NewsMedia();
        $news_resized_media->name = $news_media->name;
        $news_resized_media->original_name = $news_media->original_name;
        $news_resized_media->type = $size;
        $news_resized_media->details = json_encode([
            'width' => $resized_media->width(),
            'height' => $resized_media->height()
        ]);
        $news_resized_media->extension = $news_media->extension;
        $news_resized_media->dynamic_id = $news_media->dynamic_id;
        $news_resized_media->mimetype = $news_media->mimetype;
        $news_resized_media->size = $resized_media->filesize();
        $news_resized_media->sort = $news_media->sort;
        $news_resized_media->parent_id = $news_media->id;
        $news_resized_media->news_id = $news->id;
        $news_resized_media->save();

        return $news_media;
    }
}
