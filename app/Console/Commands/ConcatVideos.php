<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ConcatVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concat:videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concat Videos for News';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $video_for_concat_resized_filename = 'resized_' . $video_for_concat_filename;

        $video_resize = 'ffmpeg -i ' . $app_path . $video_for_concat_filename . ' -c:v libx264 -pix_fmt yuv420p -crf 23 -framerate 24 -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2,setsar=1" ' . $app_path . $video_for_concat_resized_filename;
    }
}
