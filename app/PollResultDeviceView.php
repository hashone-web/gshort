<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollResultDeviceView extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'poll_id',
    ];
}
