<?php

namespace App\Http\Controllers\News;

use App\Category;
use App\Http\Controllers\Controller;
use App\News;
use App\NewsMedia;
use Illuminate\Http\Request;

use App\Events\NewsPublished;
use App\Events\NewsFeatured;

use App\UserFcmToken;
use App\User;
use App\NewsSource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use ProtoneMedia\LaravelFFMpeg\Filesystem\Media;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $is_news_useful_page = \Request::route()->getName() == 'news.useful';

        $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
        $categories = Category::all();

        $query_parameters = $request->query();

        $news = News::with(['source', 'categories', 'thumbs.parent', 'created_by_user', 'updated_by_user', 'notifications'])
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS bookmarks FROM device_bookmarks GROUP BY news_id) as nb'), 'nb.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nb.bookmarks as bookmarks_count')
            ->addSelect('nv.views as views')
            ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');

        if($is_news_useful_page) {
            $news = $news->where('is_useful', 1);
        }

        if($request->input('is_draft') !== null) {
            $news = $news->where('status', 'Draft');
        }

        if($request->input('is_pending') !== null) {
            $news = $news->where('status', 'Pending');
        }

        if($request->input('is_unpublished') !== null) {
            $news = $news->where('status', 'Unpublished');
        }

        if($request->input('is_rejected') !== null) {
            $news = $news->where('status', 'Rejected');
        }

        if($request->input('is_draft') == null && $request->input('is_pending') == null && $request->input('is_unpublished') == null && $request->input('is_rejected') == null) {
            $news = $news->where('status', 'Published');
        }

        if($request->input('is_notification_scheduled') !== null) {
            $news = $news->where('is_notification_scheduled', 1);
            $news = $news->where('notification_datetime', '>', \Carbon::now());
        }

        if($request->input('is_breaking') !== null) {
            $news = $news->where('is_breaking', 1);
        }

        if($request->input('is_featured') !== null) {
            $news = $news->where('is_featured', 1);
        }

        if($request->input('status') !== null) {
            $news = $news->where('status', $request->input('status'));
        }

        if($request->input('category') !== null) {
            $news = $news->whereHas('categories', function($q) use($request) {
                $q->where('categories.id', $request->input('category'));
            });
        }

        if(!auth()->user()->can('view all news')) {
            $news = $news->where('created_by', auth()->id());
        }

        $news = $news->sortable(['datetime' => 'desc'])
            ->paginate(\Config::get('constants.pagination_size'))
            ->appends(request()->query());

        $counts = [];
        $counts['draft'] = News::where('status', 'Draft')->count();
        $counts['pending'] = News::where('status', 'Pending')->count();
        $counts['pending_important'] = News::where('status', 'Pending')->where('is_important', 1)->count();
        $counts['notification_scheduled'] = News::where('is_notification_scheduled', 1)->where('notification_datetime', '>', \Carbon::now())->where('status', 'Published')->count();
        $counts['important'] = News::where('is_important', 1)->where('status', 'Published')->count();
        $counts['featured'] = News::where('is_featured', 1)->where('status', 'Published')->count(); 

        return view('pages.news.index', compact('news', 'is_news_useful_page', 'query_parameters', 'statuses', 'categories', 'counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 1)->orderBy('status', 'desc')->orderBy('sort', 'asc')->get();
        
        $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
        $notification_types = \Config::get('constants.notification_types');

        return view('pages.news.create', compact('statuses', 'categories', 'notification_types'));
    }

    public function reportingCities(Request $request, $id) {
        // $user = User::with('reporting_cities')->find($id);

        // return view('partials.news.reporting_cities')->with(['cities' => $user->reporting_cities]);
        $cities = City::all();

        return view('partials.news.reporting_cities')->with(['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300); // 5 minutes

        $validator = Validator::make($request->all(), [
            'headline' => ['required', 'string', 'max:100'],
            'article' => ['nullable', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('news.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $news = new News();
            $news->headline = $request->input('headline');
            $news->article = $request->input('article');
            $news->status = $request->input('status');
            if($request->input('notification_type') !== null) {
                $news->notification_type = $request->input('notification_type');
            }
            $news->is_breaking = $request->input('is_breaking') !== null? true: false;
            $news->is_important = $request->input('is_important') !== null? true: false;
            $news->is_featured = $request->input('is_featured') !== null? true: false;
            if($request->input('datetime_current') !== null) {
                $news->datetime = \Carbon::now();
            } else {
                $news->datetime = \Carbon::createFromFormat('Y-m-d H:i', $request->input('datetime'))->format('Y-m-d H:i:s');
            }
            $news->source_link = $request->input('source_link');

            if($news->source_link !== null) {
                $source_domain_full = getTopLevelDomain($news->source_link);
                $source_domain = explode('.', $source_domain_full);
                if(count($source_domain) > 1) {
                    $source_domain = $source_domain[0];
                }
                if($source_domain) {
                    $news_source = NewsSource::firstOrNew(['domain' => $source_domain]);
                    if(!$news_source->exists) {
                        $news_source->name = ucfirst($source_domain);
                        $news_source->domain = $source_domain;
                        $news_source->save();
                    }
                    $news->news_source_id = $news_source->id;
                }
            }

            $news->video_url = $request->input('video_url');
            $news->created_by = \Auth::id();
            $news->updated_by = \Auth::id();

            $send_notification = $request->input('send_notification') !== null? true: false;

            if($news->is_breaking) {
                $news->is_breaking_datetime = \Carbon::now();
            }
            if($news->is_featured) {
                $news->is_featured_datetime = \Carbon::now();
            }

            if($news->status == 'Published') {
                $news->published_datetime = \Carbon::now();
            }

            $news->save();

            $news->categories()->sync($request->input('categories'));

            $videos_for_concat = [];

            if($request->file('media') !== null && count($request->file('media'))) {
                $all_media = $request->file('media');

                $media_data = $this->generateMediaData($request);

                foreach ($all_media as $media_index => $media) {
                    $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index, $media_data);
                    if($news_media !== null) {
                        $this->mediaUpload($news, $news_media, $media, $media_data);
                    } else {
                        array_push($videos_for_concat, $media);
                    }
                }

                if(count($videos_for_concat)) {
                    $videos_for_concat_paths = [];

                    $public_storage_path = 'app/public/';
                    $path = 'news/' . $news->id . '/media/concat_original/';
                    $app_path = storage_path($public_storage_path . $path);

                    foreach ($videos_for_concat as $video_for_concat) {
                        if (!file_exists($app_path)) {
                            \File::makeDirectory($app_path, 0777, true);
                        }

                        $video_for_concat_filename = $video_for_concat->getClientOriginalName();

                        $video_for_concat->move($app_path, $video_for_concat_filename);

                        $video_for_concat_resized_filename = 'resized_' . $video_for_concat_filename;

                        $video_resize = 'ffmpeg -i ' . $app_path . $video_for_concat_filename . ' -c:v libx264 -pix_fmt yuv420p -crf 23 -framerate 24 -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2,setsar=1" ' . $app_path . $video_for_concat_resized_filename;

                        // dd($video_resize);

                        exec($video_resize);

                        array_push($videos_for_concat_paths, $path . $video_for_concat_resized_filename);
                    }

                    $format = new \FFMpeg\Format\Video\X264('aac', 'libx264');
                    $format->setKiloBitrate(800);

                    \FFMpeg::fromDisk('public')
                        ->open($videos_for_concat_paths)
                        ->export()
                        ->inFormat($format)
                        ->concatWithTranscoding($hasVideo = true, $hasAudio = true)
                        ->save('concat.mp4');
                }
            }
            DB::commit();

            $toast_message = 'News Created successfully';

            if($news->status == 'Published' && $send_notification) {
                $data = [
                    'id' => $news->id,
                    'title' => $news->headline,
                    'content' => $news->article,
                    'priority' => $request->input('high_priority_notification') !== null? 10: 1,
                    'additional_data' => [
                        'type' => 'news',
                        'id' => $news->id,
                        'notification_type' => 0
                    ]
                ];

                if($request->input('notification_type') !== null) {
                    $notification_types = \Config::get('constants.notification_types');
                    if(isset($notification_types[$request->input('notification_type')])) {
                        $data['additional_data']['notification_type'] = $notification_types[$request->input('notification_type')];
                    }
                }

                $news->load('thumbs');

                $thumb = $news->thumbs->first();
                if($thumb) {
                    $data['big_picture'] = $thumb->full_path;
                }

                $notification_datetime = \Carbon::now();

                if($request->input('send_after') !== null) {
                    $data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i', $request->input('send_after'))->format('Y-m-d H:i:s');
                    $notification_datetime = $data['send_after'];
                    $news->is_notification_scheduled = 1;
                }

                if(config('app.env') === 'production') {
                    $included_segments = env('INCLUDED_SEGMENTS', 'Android Test');
                    $included_segments = [$included_segments];
                    $this->onesignalNotification($data, $included_segments);
                }

                $news->notification_status = 'Sent';
                $news->notification_datetime = $notification_datetime;
                $news->update();

                $toast_message = 'News Created successfully with Notification';
            }

            $news_for_elastic_search = $this->findNewsForElasticSearch($news->id);

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::index([
                    'id' => $news->id,
                    'index' => 'short_news',
                    'body' => $news_for_elastic_search,
                ]);
            }

            \Toastr::success($toast_message, 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating news', 'Error', []);
        }

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::with('categories', 'thumbs.parent', 'downloads', 'created_by_user', 'updated_by_user')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
            ->addSelect('nrh.reactions_happy_count as reactions_happy_count')
            ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
            ->addSelect('nv.views as views')
            ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count')->find($id);

        if($news) {
            return view('pages.news.show', compact('news'));
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::with('categories', 'original_media')->find($id);

        if($news) {
            $categories = Category::where('status', 1)->orderBy('status', 'desc')->orderBy('sort', 'asc')->get();
            
            $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
            $notification_types = \Config::get('constants.notification_types');

            return view('pages.news.create', compact('statuses', 'categories', 'news', 'notification_types'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ini_set('memory_limit', '-1');

        $media_data = $this->generateMediaData($request);

        $news = News::find($id);

        if($news) {
            $validator = Validator::make($request->all(), [
                'headline' => ['required', 'string', 'max:100'],
                'article' => ['nullable', 'string'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('news.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $old_is_breaking = $news->is_breaking;
                $old_is_featured = $news->is_featured;

                $news->headline = $request->input('headline');
                $news->article = $request->input('article');

                if($news->status !== $request->input('status') && $request->input('status') == 'Published') {
                    $news->published_datetime = \Carbon::now();
                }

                if($request->input('notification_type') !== null && $news->notification_type !== $request->input('notification_type')) {
                    $news->notification_type = $request->input('notification_type');
                }

                $news->status = $request->input('status');
                $news->is_breaking = $request->input('is_breaking') !== null? true: false;
                $news->is_important = $request->input('is_important') !== null? true: false;
                $news->is_featured = $request->input('is_featured') !== null? true: false;
                if($request->input('datetime_current') !== null) {
                    $news->datetime = \Carbon::now();
                } else {
                    $news->datetime = \Carbon::createFromFormat('Y-m-d H:i', $request->input('datetime'))->format('Y-m-d H:i:s');
                }
                $news->source_link = $request->input('source_link');

                if($news->source_link !== null) {
                    $source_domain_full = getTopLevelDomain($news->source_link);
                    $source_domain = explode('.', $source_domain_full);
                    if(count($source_domain) > 1) {
                        $source_domain = $source_domain[0];
                    }
                    if($source_domain) {
                        $news_source = NewsSource::firstOrNew(['domain' => $source_domain]);
                        if(!$news_source->exists) {
                            $news_source->name = ucfirst($source_domain);
                            $news_source->domain = $source_domain;
                            $news_source->save();
                        }
                        $news->news_source_id = $news_source->id;
                    }
                }
                
                $news->video_url = $request->input('video_url');
                $news->updated_by = \Auth::id();

                $send_notification = $request->input('send_notification') !== null? true: false;

                if($old_is_breaking == 0 && $news->is_breaking) {
                    $news->is_breaking_datetime = \Carbon::now();
                }
                if($old_is_featured == 0 && $news->is_featured) {
                    $news->is_featured_datetime = \Carbon::now();
                }

                $news->update();

                $news->categories()->sync($request->input('categories'));

                if($request->file('media') !== null && count($request->file('media'))) {
                    $all_media = $request->file('media');

                    foreach ($all_media as $media_index => $media) {
                        $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index, $media_data);
                        $this->mediaUpload($news, $news_media, $media, $media_data);
                    }
                }

                foreach ($media_data as $media_data_key => $media_data_value) {
                    if(strpos($media_data_key, 'existing__') === 0) {
                        $media_id = (int) str_replace('existing__', '', $media_data_key);
                        $this->updateExistingMedia($news->id, $media_id, $media_data_value);
                    }
                }

                DB::commit();

                $toast_message = 'News Updated successfully';

                if($news->notification_status == 'Pending' && $news->status == 'Published' && $send_notification) {
                    $data = [
                        'id' => $news->id,
                        'title' => $news->headline,
                        'content' => $news->article,
                        'priority' => $request->input('high_priority_notification') !== null? 10: 1,
                        'additional_data' => [
                            'type' => 'news',
                            'id' => $news->id,
                            'notification_type' => 0
                        ]
                    ];

                    if($request->input('notification_type') !== null) {
                        $notification_types = \Config::get('constants.notification_types');
                        if(isset($notification_types[$request->input('notification_type')])) {
                            $data['additional_data']['notification_type'] = $notification_types[$request->input('notification_type')];
                        }
                    }

                    $news->load('thumbs');

                    $thumb = $news->thumbs->first();
                    if($thumb) {
                        $data['big_picture'] = $thumb->full_path;
                    }

                    $notification_datetime = \Carbon::now();

                    if($request->input('send_after') !== null) {
                        $data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i', $request->input('send_after'))->format('Y-m-d H:i:s');
                        $notification_datetime = $data['send_after'];
                        $news->is_notification_scheduled = 1;
                    }

                    if(config('app.env') === 'production') {
                        $included_segments = env('INCLUDED_SEGMENTS', 'Android Test');
                        $included_segments = [$included_segments];
                        $this->onesignalNotification($data, $included_segments);
                    }

                    $news->notification_status = 'Sent';
                    $news->notification_datetime = $notification_datetime;
                    $news->update();

                    $toast_message = 'News Updated successfully with Notification';
                }

                $news_for_elastic_search = $this->findNewsForElasticSearch($news->id);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $news->id,
                        'index' => 'short_news',
                        'body' => [
                            'doc' => $news_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                \Toastr::success($toast_message, 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during updating news', 'Error', []);
            }

            return redirect()->route('home');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        if($news) {
            $news->delete();

            \Elasticsearch::delete([
                'id' => $news->id,
                'index' => 'short_news'
            ]);


            \Toastr::success('News Deleted successfully', 'Success', []);

            return redirect()->back();
        }

        abort(404);
    }
}
