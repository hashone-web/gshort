<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\News;
use App\NewsMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class NewsMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::with('media')->find($id);

        if($news) {
            return view('pages.news.media', compact('news'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ini_set('memory_limit', '-1');
        
        $news = News::find($id);

        if($news) {
            try {
                DB::beginTransaction();

                if($request->file('media') !== null && count($request->file('media'))) {
                    $all_media = $request->file('media');
                    foreach ($all_media as $media_index => $media) {
                        $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index);
                        $this->mediaUpload($news, $news_media, $media);
                    }
                }

                $all_requests = $request->all();
                $all_requests = array_keys($all_requests);


                // Is Sensitive 

                $is_sensitive_media_keys = array_filter($all_requests, function($all_request) {
                    return strpos($all_request, 'is_sensitive__') === 0;
                });

                $is_sensitive_media_ids = array_map(function($is_sensitive_media_key) {
                    return (int) str_replace('is_sensitive__', '', $is_sensitive_media_key);
                }, $is_sensitive_media_keys);
                $is_sensitive_all_media_ids_1 = NewsMedia::whereIn('parent_id', $is_sensitive_media_ids)->get()->pluck('id')->toArray();
                $is_sensitive_all_media_ids_2 = NewsMedia::whereIn('parent_id', $is_sensitive_all_media_ids_1)->get()->pluck('id')->toArray();
                $is_sensitive_media_ids = array_merge($is_sensitive_media_ids, $is_sensitive_all_media_ids_1, $is_sensitive_all_media_ids_2);

                $all_media_ids = NewsMedia::where('news_id', $id)->get()->pluck('id')->toArray();

                $not_is_sensitive_media_ids = array_diff($all_media_ids, $is_sensitive_media_ids);

                NewsMedia::whereIn('id', $is_sensitive_media_ids)->update(['is_sensitive' => true]);
                NewsMedia::whereIn('id', $not_is_sensitive_media_ids)->update(['is_sensitive' => false]);


                // Is File Photo
                
                $is_file_photo_media_keys = array_filter($all_requests, function($all_request) {
                    return strpos($all_request, 'is_file_photo__') === 0;
                });

                $is_file_photo_media_ids = array_map(function($is_file_photo_media_key) {
                    return (int) str_replace('is_file_photo__', '', $is_file_photo_media_key);
                }, $is_file_photo_media_keys);
                $is_file_photo_all_media_ids_1 = NewsMedia::whereIn('parent_id', $is_file_photo_media_ids)->get()->pluck('id')->toArray();
                $is_file_photo_all_media_ids_2 = NewsMedia::whereIn('parent_id', $is_file_photo_all_media_ids_1)->get()->pluck('id')->toArray();
                $is_file_photo_media_ids = array_merge($is_file_photo_media_ids, $is_file_photo_all_media_ids_1, $is_file_photo_all_media_ids_2);

                $all_media_ids = NewsMedia::where('news_id', $id)->get()->pluck('id')->toArray();

                $not_is_file_photo_media_ids = array_diff($all_media_ids, $is_file_photo_media_ids);

                NewsMedia::whereIn('id', $is_file_photo_media_ids)->update(['is_file_photo' => true]);
                NewsMedia::whereIn('id', $not_is_file_photo_media_ids)->update(['is_file_photo' => false]);


                // Courtesy

                $courtesy_media_keys = array_filter($all_requests, function($all_request) {
                    return strpos($all_request, 'courtesy__') === 0;
                });

                foreach ($courtesy_media_keys as $courtesy_media_key) {
                    $courtesy_media_id = (int) str_replace('courtesy__', '', $courtesy_media_key);
                    $courtesy_all_media_ids_1 = NewsMedia::where('parent_id', $courtesy_media_id)->get()->pluck('id')->toArray();
                    $courtesy_all_media_ids_2 = NewsMedia::whereIn('parent_id', $courtesy_all_media_ids_1)->get()->pluck('id')->toArray();
                    $courtesy_media_ids = array_merge([ $courtesy_media_id ], $courtesy_all_media_ids_1, $courtesy_all_media_ids_2);

                    NewsMedia::whereIn('id', $courtesy_media_ids)->update(['courtesy' => $request->input($courtesy_media_key)]);
                }

                if($request->input('media_sorting') !== null) {
                    try {
                        $media_sorting = $request->input('media_sorting');
                        $media_sorting = json_decode($media_sorting, true);

                        foreach ($media_sorting as $media_sorting_index => $media_sorting_single) {
                            $media_ids = [];
                            array_push($media_ids, $media_sorting_single);
                            $media_ids_1 = NewsMedia::whereIn('parent_id', $media_ids)->get()->pluck('id')->toArray();
                            $media_ids_2 = NewsMedia::whereIn('parent_id', $media_ids_1)->get()->pluck('id')->toArray();
                            $final_media_ids = array_merge($media_ids, $media_ids_1, $media_ids_2);
                            NewsMedia::whereIn('id', $final_media_ids)->update(['sort' => $media_sorting_index]);
                        }
                    } catch (\Exception $e) {

                    }
                }

                $news->updated_by = \Auth::id();
                $news->update();

                DB::commit();

                \Toastr::success('News Media Uploaded successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during news media upload', 'Error', []);
            }

            return redirect()->route('home');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news_media = NewsMedia::find($id);

        if($news_media) {
            $news_media_ids_1 = NewsMedia::where('parent_id', $news_media->id)->get()->pluck('id')->toArray();
            $news_media_ids_2 = NewsMedia::whereIn('parent_id', $news_media_ids_1)->get()->pluck('id')->toArray();

            NewsMedia::whereIn('id', $news_media_ids_2)->delete();
            NewsMedia::whereIn('id', $news_media_ids_1)->delete();
            $news_media->delete();
        }

        return [
            'status' => true
        ];
    }
}
