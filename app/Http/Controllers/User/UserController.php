<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use App\UserFcmToken;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::all();
        $statuses = ['Pending', 'Active', 'Inactive', 'Rejected', 'Blocked'];

        $query_parameters = $request->query();

        $users = User::sortable(['id' => 'desc']);

        if($request->input('role') !== null) {
            $users = $users->role($request->input('role'));
        }

        if($request->input('status') !== null) {
            $users = $users->where('status', $request->input('status'));
        }

        if($request->input('search_type') !== null && $request->input('search_value') !== null) {
            if($request->input('search_type') == 'name') {
                $users = $users->where(function($query) use($request) {
                    return $query->where('name', 'like', '%'. $request->input('search_value') . '%')
                        ->orWhere('alternate_name', 'like', '%'. $request->input('search_value') . '%');
                });
            } else {
                $users = $users->where($request->input('search_type'), 'like', '%'. $request->input('search_value') . '%');
            }
        }

        $users = $users->paginate(\Config::get('constants.pagination_size'))
            ->appends(request()->query());

        return view('pages.users.index', compact('users', 'roles', 'statuses', 'query_parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $statuses = ['Pending', 'Active', 'Inactive', 'Rejected', 'Blocked'];

        return view('pages.users.create', compact('roles', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'mobile_number' => ['required', 'digits:10', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('users.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $user = new User();
            $user->name = $request->input('name');
            $user->mobile_number = $request->input('mobile_number');
            $user->type = 'Admin';
            $user->status = $request->input('status');
            $user->password = $request->input('password');
            $user->save();

            $user->assignRole($request->input('role'));

            if($request->input('role') == 'Reporter') {
                $user->alternate_name = $request->input('alternate_name');
                $user->anonymous_reporting = $request->input('anonymous_reporting') == 'yes'? 1: 0;
            }

            if ($request->file('profile_picture') != null) {
                $image = $request->input('profile_picture_data64');
                $profile_picture_path = public_path('/storage/users/' . $user->id . '/profile_picture/');

                if (!file_exists($profile_picture_path)) {
                    \File::makeDirectory($profile_picture_path, 0777, true);
                } else {
                    \File::deleteDirectory($profile_picture_path, 0777, true);
                    if (!file_exists($profile_picture_path)) {
                        \File::makeDirectory($profile_picture_path, 0777, true);
                    }
                }

                $profile_picture_file = Str::random(10) . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($image)->save($profile_picture_path . $profile_picture_file);

                $this->createResizedImage($profile_picture_path, $image, $profile_picture_file, false);

                $user->profile_picture = $profile_picture_file;
                $user->update();
            }
            DB::commit();

            \Toastr::success('User Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating user', 'Error', []);
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $statuses = ['Pending', 'Active', 'Inactive', 'Rejected', 'Blocked'];

        $user = User::find($id);

        if($user) {
            return view('pages.users.create', compact('roles', 'statuses', 'user'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if($user) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'mobile_number' => ['required', 'digits:10', 'unique:users,mobile_number,' . $id],
                'password' => ['nullable', 'string', 'min:6', 'confirmed'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('users.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $user->name = $request->input('name');
                $user->mobile_number = $request->input('mobile_number');
                if($request->input('status') !== null) {
                    $user->status = $request->input('status');
                }
                if($request->input('password') !== null) {
                    $user->password = $request->input('password');
                }
                $user->type = 'Admin';
                $user->update();

                if($request->input('role') !== null) {
                    if(count($user->getRoleNames())) {
                        if($request->input('role') !== $user->getRoleNames()->first()) {
                            $user->removeRole($user->getRoleNames()->first());
                            $user->assignRole($request->input('role'));
                        }
                    } else {
                        $user->assignRole($request->input('role'));
                    }
                }

                if ($request->file('profile_picture') != null) {
                    $image = $request->input('profile_picture_data64');
                    $profile_picture_path = public_path('/storage/users/' . $user->id . '/profile_picture/');

                    if (!file_exists($profile_picture_path)) {
                        \File::makeDirectory($profile_picture_path, 0777, true);
                    } else {
                        \File::deleteDirectory($profile_picture_path, 0777, true);
                        if (!file_exists($profile_picture_path)) {
                            \File::makeDirectory($profile_picture_path, 0777, true);
                        }
                    }

                    $profile_picture_file = Str::random(10) . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                    \Image::make($image)->save($profile_picture_path . $profile_picture_file);

                    $this->createResizedImage($profile_picture_path, $image, $profile_picture_file, false);

                    $user->profile_picture = $profile_picture_file;
                    $user->update();
                }
                DB::commit();

                \Toastr::success('User Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during updating user', 'Error', []);
            }

            return redirect()->back();
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileEdit()
    {
        $id = \Auth::id();

        $roles = ['Admin', 'Moderator', 'Content Creator', 'Reporter'];
        $statuses = ['Pending', 'Active', 'Inactive', 'Rejected', 'Blocked'];
        $user = User::find($id);
        $profile_edit = true;

        if($user) {
            return view('pages.users.create', compact('roles', 'statuses', 'user', 'profile_edit'));
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user) {
            $user->delete();

            \Toastr::success('User Deleted successfully', 'Success', []);

            return redirect()->route('users.index');
        }

        abort(404);
    }

    public function fcmToken(Request $request, $id)
    {
        $token = $request->input('token');

        $user_fcm_token = UserFcmToken::firstOrNew(['user_id' => $id, 'token' => $token]);
        $user_fcm_token->save();

        return [
            'status' => true
        ];
    }

    public function fcmTest()
    {
        $tokens = UserFcmToken::where('user_id', \Auth::id())->first()->pluck('token')->toArray();

        dd('done', $tokens);
    }
}
