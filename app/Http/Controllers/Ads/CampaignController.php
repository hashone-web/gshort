<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Campaign;
use App\CampaignBanner;
use App\Advertiser;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::with('advertiser')->withCount(['reactions', 'shares', 'clicks', 'impressions' => function ($query) {
            $query->select(DB::raw("SUM(impressions) as impressions_count"))->where('campaign_device_impressions.impression_date', '!=', '0000-00-00');
        }])->sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.ads.campaigns.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $advertisers = Advertiser::all();
        $statuses = ['Active', 'Inactive'];

        return view('pages.ads.campaigns.create', compact('advertisers', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:campaigns'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('campaigns.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $campaign = new Campaign();
            $campaign->name = $request->input('name');
            $campaign->advertiser_id = $request->input('advertiser');
            $campaign->budget = $request->input('budget');
            $campaign->start_date = $request->input('start_date');
            $campaign->end_date = $request->input('end_date');
            $campaign->notes = $request->input('notes');
            $campaign->status = $request->input('status');
            $campaign->save();

            if($request->file('banner') !== null && count($request->file('banner'))) {
                $all_banner = $request->file('banner');
                $banner_data = $this->generateBannerData($request);

                foreach ($all_banner as $banner_key => $banner) {
                    $banner_unique_id = preg_replace('/[\W_]+/u', '', $banner->getClientOriginalName());
                    $banner_unique_id = substr($banner_unique_id, 0, 10);
                    $banner_unique_id = $banner->getSize() . '' . $banner_unique_id;

                    $campaign_banner = new CampaignBanner();
                    $campaign_banner->type = $banner_data[$banner_unique_id]['type'];
                    $campaign_banner->action = $banner_data[$banner_unique_id]['action'];
                    $campaign_banner->action_data = $banner_data[$banner_unique_id]['action_data'];
                    $campaign_banner->action_text = $banner_data[$banner_unique_id]['action_text'];
                    $campaign_banner->action_button_colour = $banner_data[$banner_unique_id]['action_button_colour'];
                    $campaign_banner->action_text_colour = $banner_data[$banner_unique_id]['action_text_colour'];
                    $campaign_banner->campaign_id = $campaign->id;
                    $campaign_banner->save();

                    $public_storage_path = 'app/public/';
                    $path = 'campaigns/' . $campaign->id . '/banners/' . $campaign_banner->id;
                    $app_path = storage_path($public_storage_path . $path);

                    if (!file_exists($app_path)) {
                        \File::makeDirectory($app_path, 0777, true);
                    }

                    $banner_extension = $banner->getClientOriginalExtension();
                    $banner_filename = \Str::random(16) . '.' . $banner_extension;
                    $banner->move($app_path, $banner_filename);

                    $campaign_banner->media = $banner_filename;
                    $campaign_banner->media_type = 'image';

                    if(isset($banner_data[$banner_unique_id]['action_image'])) {
                        $app_path = $app_path . '/action_image';
                        if (!file_exists($app_path)) {
                            \File::makeDirectory($app_path, 0777, true);
                        }

                        $banner_action_image = $banner_data[$banner_unique_id]['action_image'];
                        $banner_action_image_extension = $banner_action_image->getClientOriginalExtension();
                        $banner_action_image_filename = \Str::random(16) . '.' . $banner_action_image_extension;
                        $banner_action_image->move($app_path, $banner_action_image_filename);
                    
                        $campaign_banner->action_image = $banner_action_image_filename;
                    }

                    $campaign_banner->update();
                }
            }

            // $this->campaignCache($campaign->id);

            DB::commit();

            \Toastr::success('Campaign Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating campaign', 'Error', []);
        }

        return redirect()->route('campaigns.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campaign = Campaign::with('banners')->find($id);

        if($campaign) {
            $advertisers = Advertiser::all();
            $statuses = ['Active', 'Inactive'];

            return view('pages.ads.campaigns.create', compact('campaign', 'advertisers', 'statuses'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campaign = Campaign::find($id);

        if($campaign) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:campaigns,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('campaigns.create', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $banner_data = $this->generateBannerData($request);

                $campaign->name = $request->input('name');
                $campaign->advertiser_id = $request->input('advertiser');
                $campaign->budget = $request->input('budget');
                $campaign->start_date = $request->input('start_date');
                $campaign->end_date = $request->input('end_date');
                $campaign->notes = $request->input('notes');
                $campaign->status = $request->input('status');
                $campaign->update();

                if($request->file('banner') !== null && count($request->file('banner'))) {
                    $all_banner = $request->file('banner');

                    foreach ($all_banner as $banner_key => $banner) {
                        $banner_unique_id = preg_replace('/[\W_]+/u', '', $banner->getClientOriginalName());
                        $banner_unique_id = substr($banner_unique_id, 0, 10);
                        $banner_unique_id = $banner->getSize() . '' . $banner_unique_id;

                        $campaign_banner = new CampaignBanner();
                        $campaign_banner->type = $banner_data[$banner_unique_id]['type'];
                        $campaign_banner->action = $banner_data[$banner_unique_id]['action'];
                        $campaign_banner->action_data = $banner_data[$banner_unique_id]['action_data'];
                        $campaign_banner->action_text = $banner_data[$banner_unique_id]['action_text'];
                        $campaign_banner->action_button_colour = $banner_data[$banner_unique_id]['action_button_colour'];
                        $campaign_banner->action_text_colour = $banner_data[$banner_unique_id]['action_text_colour'];
                        $campaign_banner->campaign_id = $campaign->id;
                        $campaign_banner->save();

                        $public_storage_path = 'app/public/';
                        $path = 'campaigns/' . $campaign->id . '/banners/' . $campaign_banner->id;
                        $app_path = storage_path($public_storage_path . $path);

                        if (!file_exists($app_path)) {
                            \File::makeDirectory($app_path, 0777, true);
                        }

                        $banner_extension = $banner->getClientOriginalExtension();
                        $banner_filename = \Str::random(16) . '.' . $banner_extension;
                        $banner->move($app_path, $banner_filename);

                        $campaign_banner->media = $banner_filename;
                        $campaign_banner->media_type = 'image';

                        if(isset($banner_data[$banner_unique_id]['action_image'])) {
                            $app_path = $app_path . '/action_image';
                            if (!file_exists($app_path)) {
                                \File::makeDirectory($app_path, 0777, true);
                            }

                            $banner_action_image = $banner_data[$banner_unique_id]['action_image'];
                            $banner_action_image_extension = $banner_action_image->getClientOriginalExtension();
                            $banner_action_image_filename = \Str::random(16) . '.' . $banner_action_image_extension;
                            $banner_action_image->move($app_path, $banner_action_image_filename);
                        
                            $campaign_banner->action_image = $banner_action_image_filename;
                        }

                        $campaign_banner->update();
                    }
                }

                foreach ($banner_data as $banner_data_key => $banner_data_value) {
                    if(strpos($banner_data_key, 'existing__') === 0) {
                        $banner_id = (int) str_replace('existing__', '', $banner_data_key);
                        $this->updateExistingBanner($campaign->id, $banner_id, $banner_data_value);
                    }
                }

                // $this->campaignCache($campaign->id);

                DB::commit();

                \Toastr::success('Campaign Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating campaign', 'Error', []);
            }

            return redirect()->route('campaigns.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);

        if($campaign) {
            $campaign->delete();

            \Toastr::success('Campaign Deleted successfully', 'Success', []);

            return redirect()->route('campaigns.index');
        }

        abort(404);
    }

    private function campaignCache($id)
    {
        $campaign = Campaign::with('banners')->find($id);
        if($campaign) {
            foreach ($campaign->banners as $banner) {
                $banner_data = $banner->toArray();
                $banner_data['campaign_start_date'] = $campaign->start_date;
                $banner_data['campaign_end_date'] = $campaign->end_date;
                $banner_data['campaign_status'] = $campaign->status;
                $banner_data['campaign_deleted_at'] = $campaign->deleted_at === null? 0: 1;
                
                \Elasticsearch::update([
                    'id' => $banner_data['id'],
                    'index' => 'short_ads',
                    'body' => [
                        'doc' => $banner_data,
                        'doc_as_upsert' => true,
                    ]
                ]);
            }
        }

        return $campaign;
    }
}
