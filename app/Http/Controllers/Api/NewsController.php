<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Validator;
use Carbon\Carbon;

use App\Device;
use App\UserDevice;
use App\DeviceLocation;
use App\Report;
use App\ReportType;
use App\Setting;
use App\Update;
use App\News;
use App\NewsStatistic;
use App\User;
use App\Poll;
use App\PollOption;
use App\DevicePollOption;
use App\NewsNotification;
use App\Advertiser;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $now = \Carbon::now()->format('Y-m-d H:i:s');
        $poll = null;
        $topic = $request->input('topic') !== null? $request->input('topic'): 'feed';

        $pagination_size = $request->query('limit') !== null? (int) $request->query('limit'): \Config::get('constants.pagination_size');
        $page = $request->input('page') !== null? (int) $request->input('page'): 1;
        $from = ($page - 1) * $pagination_size;

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $device_data = [
            'device' => [ 
                'id' => $device->id,
                'unique_id' => $device->unique_id
            ]
        ];

        Redis::set('device_data:' . $device->unique_id, json_encode($device_data));       

        $device_data = Redis::get('device_data:'. $request->input('device_id'));

        if($device_data !== null) {
            $device_data = json_decode($device_data);

            $device = $device_data->device;

            Device::where('id', $device->id)
                ->update([
                    'last_active_at' => $now
                ]);

            if($request->input('page') !== null && $request->input('page') == 1) {
                $poll = Poll::with('options')->where('type', 'Standalone')->where('status', 'Active')
                    ->where('start_datetime', '<', $now)
                    ->where(function($query) use($now) {
                        return $query->where('end_datetime', '>', $now)
                            ->orWhere(function($query) use($now) {
                                return $query->where('show_result', '!=', 'Never')
                                    ->where('result_end_datetime', '>', $now);
                            });
                    })
                    ->first();  

                if($poll) {
                    $poll = $this->pollResponce($device, $poll);
                }
            }

            if(env('ELASTICSEARCH')) {
                $elastic_search_params = '{
                    "index":"short_news",
                    "body":{
                        "query":{
                            "bool":{
                                "must": [ 
                                    {
                                        "match": {
                                            "status":"Published"
                                        }
                                    },
                                    {
                                        "range":{
                                            "datetime":{
                                                "lte":"'. $now .'"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "sort":{
                            "datetime":{
                                "order":"desc"
                            }
                        }
                    },
                    "from":'. $from .',
                    "size":'. $pagination_size .'
                }';

                $elastic_search_params = json_decode($elastic_search_params, true);

                if($request->query('last_request_ts') !== null) {
                    $last_request_ts = ($request->query('last_request_ts') / 1000) - (60);
                    $last_request_ts = Carbon::createFromTimestamp($last_request_ts)->toDateTimeString();

                    array_push($elastic_search_params['body']['query']['bool']['must'], [
                        'range' => [
                            'datetime' => [
                                'gte' => $last_request_ts
                            ]
                        ]
                    ]);
                }

                if($request->query('last_news_ts') !== null) {
                    $last_news_ts = $request->query('last_news_ts');

                    array_push($elastic_search_params['body']['query']['bool']['must'], [
                        'range' => [
                            'datetime' => [
                                'lt' => $last_news_ts
                            ]
                        ]
                    ]);
                }

                $topic_id = (int) $topic;

                if($topic_id) {
                    $elastic_search_params['body']['query']['bool']['filter'] = [
                        'terms' => [
                            'categories_ids' => [ $topic_id ]
                        ]
                    ];
                }

                if($topic == 'important') {
                    array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_important' => 1]]);
                }

                if($topic == 'trending') {
                    array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_featured' => 1]]);
                }

                $raw_news = \Elasticsearch::search($elastic_search_params);
                $news = [];

                $banner_track = 0;
                $banner_now = \Carbon::now()->format('Y-m-d');

                $last_news_ts = null;

                foreach ($raw_news['hits']['hits'] as $key => $raw_news_single) {
                    $is_sensitive_viewed = 0;
                    if(in_array($device->id, $raw_news_single['_source']['sensitivities'])) $is_sensitive_viewed = 1;
                    $raw_news_single['_source']['is_sensitive_viewed'] = $is_sensitive_viewed;

                    $raw_news_single['_source']['is_bookmarked'] = 0;
                    if(in_array($device->id, $raw_news_single['_source']['bookmarks'])) $raw_news_single['_source']['is_bookmarked'] = 1;

                    unset($raw_news_single['_source']['sensitivities']);
                    unset($raw_news_single['_source']['bookmarks']);

                    if(isset($raw_news_single['_source']['poll']) && $raw_news_single['_source']['poll'] !== null) {
                        $raw_news_single['_source']['poll'] = $this->pollResponce($device, $raw_news_single['_source']['poll']); 
                    }

                    $banner_track++;

                    if($banner_track >= 6) {
                        if(rand(0,1)) {
                            $raw_news_single['_source']['online_banner'] = 1;
                            $raw_news_single['_source']['online_ads'] = 1;
                            $raw_news_single['_source']['online_ads_type'] = rand(0,1) === 0? 'native': 'rectangle';
                        } else {
                            $banner = $this->getAdsBanner('feed', $banner_now, $device->id);
                            if($banner) $raw_news_single['_source']['banner'] = $banner;
                        }
                        
                        $banner_track = 0;
                    }

                    $last_news_ts = $raw_news_single['_source']['datetime'];

                    $news[] = $raw_news_single['_source'];
                }

                $total = $raw_news['hits']['total']['value'];
                $last_page = ceil($total / $pagination_size);

                $data = [
                    'server_time' => time() * 1000,
                    'status' => true,
                    'data' => $news,
                    'last_news_ts' => $last_news_ts,
                    'poll' => $poll,
                    'elastic_search' => true,
                ];

                $data['pagination'] = [
                    'show_pagination' => ($total / $pagination_size) > 1,
                    'total' => $total,
                    'per_page' => $pagination_size,
                    'current_page' => $page,
                    'last_page' => $last_page,
                    'from' => $from + 1,
                    'to' => $from + $pagination_size
                ];

                return $data;
            }

            $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                    $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
                }])
                ->whereHas('cities', function($q) use($user_cities) {
                    $q->whereIn('news_cities.city_id', $user_cities);
                })
                // ->has('city')
                ->leftJoin(\DB::raw('(SELECT news_id, type FROM news_reactions where device_id = ' . $device->id . ' GROUP BY news_id, type) as nr'), 'nr.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS is_sensitive_viewed FROM news_device_sensitivities where device_id = ' . $device->id . ' GROUP BY news_id) as nds'), 'nds.news_id', '=', 'news.id')
                ->addSelect('news.*')
                ->addSelect('nr.type as reaction')
                ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
                ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
                ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
                ->addSelect('nrc.reactions_claps_count as views')
                ->withCount('shares as shares_whatsapp_count');
                // ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');
            
            if($request->input('platform') !== null && $request->input('platform') == 'iOS') {
                $news = $news->addSelect(\DB::raw('IFNULL(nds.is_sensitive_viewed, 0) as is_sensitive_viewed'));
            } else {
                $news = $news->addSelect('nds.is_sensitive_viewed as is_sensitive_viewed');
            }

            $news = $news->orderByDesc('news.datetime');

            // $news = $news->join('news_cities', function ($j) use($user_cities)
            // {
            //     $j->on('news.id', '=', 'news_cities.news_id');
            //     $j->whereIn('news_cities.city_id', $user_cities);
            // })->distinct()->addSelect('news.*');

            if($request->query('last_request_ts') !== null) {
                $last_request_ts = $request->query('last_request_ts') / 1000;
                $last_request_ts = Carbon::createFromTimestamp($last_request_ts)->toDateTimeString();
                $news = $news->where('news.created_at', '>', $last_request_ts);
            }

            $news = $news->where('news.datetime', '<', Carbon::now());
            $news = $news->where('news.status', 'Published');

            $news = $news->paginate($pagination_size);
            $news_data = $news->toArray();

            foreach ($news_data['data'] as $key => $news_data_single) {
                if($news_data['data'][$key]['poll'] !== null) {
                    $news_data['data'][$key]['poll'] = $this->pollResponce($device, $news_data['data'][$key]['poll']); 
                }
            }

            $data = [
                'server_time' => time() * 1000,
                'status' => true,
                'data' => $news_data['data'],
                'poll' => $poll,
            ];

            $data['pagination'] = [
                'show_pagination' => ($news->total() / $pagination_size) > 1,
                'total' => $news->total(),
                'per_page' => $news->perPage(),
                'current_page' => $news->currentPage(),
                'last_page' => $news->lastPage(),
                'from' => $news->firstItem(),
                'to' => $news->lastItem()
            ];

            return $data;
        }

        return [
            'status' => false
        ];
    }

    public function show($id, Request $request)
    {
        if(env('ELASTICSEARCH')) {
            try {
                $now = \Carbon::now()->format('Y-m-d');

                $raw_news = \Elasticsearch::get([
                    'id' => $id,
                    'index' => 'short_news',
                ]);

                $news = $raw_news['_source'];

                unset($news['cities']);
                unset($news['likes']);
                unset($news['dislikes']);
                unset($news['sensitivities']);

                if(isset($news['poll']) && $news['poll'] !== null) {
                    $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
                    $device->save();

                    $news['poll'] = $this->pollResponce($device, $news['poll']); 
                }

                $device_data = Redis::get('device_data:'. $request->input('device_id'));

                if($device_data !== null) {
                    $device_data = json_decode($device_data);

                    $banner = $this->getAdsBanner('detail', $now, $device_data->device->id);
                    if($banner) $news['banner'] = $banner;
                }

                $data = [
                    'server_time' => time() * 1000,
                    'status' => true,
                    'data' => $news,
                    'elastic_search' => true,
                ];

                return $data;
            } catch (\Exception $e) {
                if($e->getCode() == 404) {
                    return [
                        'status' => false,
                        'message' => 'data not found'
                    ];
                }
                return [
                    'status' => false
                ];
            }
        }

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $news = $this->findNews($id, $device, $request);

        if($news) {
            $news = $news->toArray();

            if($news['poll'] !== null) {
                $news['poll'] = $this->pollResponce($device, $news['poll']); 
            }

            $data = [
                'server_time' => time() * 1000,
                'status' => true,
                'data' => $news,
            ];
        }

        return $data;
    }

    public function featured_news(Request $request)
    {
        $now = \Carbon::now()->format('Y-m-d H:i:s');

        $pagination_size = $request->query('limit') !== null? (int) $request->query('limit'): \Config::get('constants.pagination_size');
        $page = $request->input('page') !== null? (int) $request->input('page'): 1;
        $from = ($page - 1) * $pagination_size;

        $device_data = Redis::get('device_data:'. $request->input('device_id'));

        if($device_data !== null) {
            $device_data = json_decode($device_data);

            $device = $device_data->device;
            $user_cities = $device_data->user_cities;

            if(env('ELASTICSEARCH')) {
                $elastic_search_params = '{
                    "index":"short_news",
                    "body":{
                        "query":{
                            "bool":{
                                "must": [ 
                                    {
                                        "match": {
                                            "status":"Published"
                                        }
                                    },
                                    {
                                        "range":{
                                            "datetime":{
                                                "lte":"'. $now .'"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "sort":{
                            "datetime":{
                                "order":"desc"
                            }
                        }
                    },
                    "from":'. $from .',
                    "size":'. $pagination_size .'
                }';

                $elastic_search_params = json_decode($elastic_search_params, true);

                $raw_news = \Elasticsearch::search($elastic_search_params);
                $news = [];

                foreach ($raw_news['hits']['hits'] as $key => $raw_news_single) {
                    $reaction = null;
                    if(in_array($device->id, $raw_news_single['_source']['likes'])) $reaction = 'Claps';
                    if(in_array($device->id, $raw_news_single['_source']['dislikes'])) $reaction = 'Sad';
                    $raw_news_single['_source']['reaction'] = $reaction;

                    $is_sensitive_viewed = $request->input('platform') !== null && $request->input('platform') == 'iOS' ? 0: null;
                    if(in_array($device->id, $raw_news_single['_source']['sensitivities'])) $is_sensitive_viewed = 1;
                    $raw_news_single['_source']['is_sensitive_viewed'] = $is_sensitive_viewed;

                    unset($raw_news_single['_source']['cities']);
                    unset($raw_news_single['_source']['likes']);
                    unset($raw_news_single['_source']['dislikes']);
                    unset($raw_news_single['_source']['sensitivities']);

                    if(isset($raw_news_single['_source']['poll']) && $raw_news_single['_source']['poll'] !== null) {
                        $raw_news_single['_source']['poll'] = $this->pollResponce($device, $raw_news_single['_source']['poll']); 
                    }

                    $news[] = $raw_news_single['_source'];
                }

                $total = $raw_news['hits']['total']['value'];
                $last_page = ceil($total / $pagination_size);

                $data = [
                    'server_time' => time() * 1000,
                    'status' => true,
                    'data' => $news,
                    'elastic_search' => true,
                ];

                $data['pagination'] = [
                    'show_pagination' => ($total / $pagination_size) > 1,
                    'total' => $total,
                    'per_page' => $pagination_size,
                    'current_page' => $page,
                    'last_page' => $last_page,
                    'from' => $from + 1,
                    'to' => $from + $pagination_size
                ];

                return $data;
            }

            $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                    $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
                }])
                ->whereHas('cities', function($q) use($user_cities) {
                    $q->whereIn('news_cities.city_id', $user_cities);
                })
                // ->has('city')
                ->leftJoin(\DB::raw('(SELECT news_id, type FROM news_reactions where device_id = ' . $device->id . ' GROUP BY news_id, type) as nr'), 'nr.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
                // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS is_sensitive_viewed FROM news_device_sensitivities where device_id = ' . $device->id . ' GROUP BY news_id) as nds'), 'nds.news_id', '=', 'news.id')
                ->addSelect('news.*')
                ->addSelect('nr.type as reaction')
                ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
                ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
                ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
                ->addSelect('nrc.reactions_claps_count as views')
                ->withCount('shares as shares_whatsapp_count');
                // ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');
            
            if($request->input('platform') !== null && $request->input('platform') == 'iOS') {
                $news = $news->addSelect(\DB::raw('IFNULL(nds.is_sensitive_viewed, 0) as is_sensitive_viewed'));
            } else {
                $news = $news->addSelect('nds.is_sensitive_viewed as is_sensitive_viewed');
            }

            $news = $news->orderByDesc('news.is_featured_datetime');
            $news = $news->where('is_featured', 1);

            // $news = $news->join('news_cities', function ($j) use($user_cities)
            // {
            //     $j->on('news.id', '=', 'news_cities.news_id');
            //     $j->whereIn('news_cities.city_id', $user_cities);
            // })->distinct()->addSelect('news.*');

            if($request->query('last_request_ts') !== null) {
                $last_request_ts = $request->query('last_request_ts') / 1000;
                $last_request_ts = Carbon::createFromTimestamp($last_request_ts)->toDateTimeString();
                $news = $news->where('news.created_at', '>', $last_request_ts);
            }

            $news = $news->where('news.datetime', '<', Carbon::now());
            $news = $news->where('news.status', 'Published');

            $news = $news->paginate($pagination_size);
            $news_data = $news->toArray();

            foreach ($news_data['data'] as $key => $news_data_single) {
                if($news_data['data'][$key]['poll'] !== null) {
                    $news_data['data'][$key]['poll'] = $this->pollResponce($device, $news_data['data'][$key]['poll']); 
                }
            }

            $data = [
                'server_time' => time() * 1000,
                'status' => true,
                'data' => $news_data['data'],
            ];

            $data['pagination'] = [
                'show_pagination' => ($news->total() / $pagination_size) > 1,
                'total' => $news->total(),
                'per_page' => $news->perPage(),
                'current_page' => $news->currentPage(),
                'last_page' => $news->lastPage(),
                'from' => $news->firstItem(),
                'to' => $news->lastItem()
            ];

            return $data;
        }

        return [
            'status' => false
        ];
    }

    public function reportTypes(Request $request) {
        $report_types = [];

        if($request->input('language_code') !== null && $request->input('language_code') == 'gu') {
            $report_types = ReportType::leftJoin(\DB::raw('(SELECT report_type_id, name FROM report_type_names where language_id = 2 GROUP BY report_type_id, name) as rtn'), 'rtn.report_type_id', '=', 'report_types.id')
                ->addSelect('report_types.*')
                ->addSelect('rtn.name as name')
                ->get();
        } else {
            $report_types = ReportType::get();
        }

        return [
            'status' => true,
            'data' => $report_types
        ];
    }

    public function settings(Request $request) {
        $setting = Redis::get('short_setting');
        $update = Redis::get('short_update');

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        if($setting !== null && $setting !== null) {
            return [
                'status' => true,
                'redis' => true,
                'settings' => json_decode($setting, true),
                'update' => json_decode($update, true),
                'notification_status' => $device->notification_level
            ];
        }

        $setting = Setting::first();
        $update = Update::first();

        Redis::set('short_setting', json_encode($setting), 'EX', 60);
        Redis::set('short_update', json_encode($update), 'EX', 60);

        return [
            'status' => true,
            'settings' => $setting,
            'update' => $update,
            'notification_status' => $device->notification_level
        ];
    }

    public function updates(Request $request) {
        

        if($update !== null) {
            return [
                'status' => true,
                'redis' => true,
                'data' => json_decode($update, true)
            ];
        }

        $update = Update::first();

        Redis::set('update', json_encode($update), 'EX', 60);

        return [
            'status' => true,
            'data' => $update
        ];
    }

    public function reports(Request $request) {
        $validator = Validator::make($request->all(), [
            'report_type_id' => 'required',
            'news_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        try {
            $report = new Report();
            $report->report_type_id = $request->input('report_type_id');
            $report->news_id = $request->input('news_id');
            $report->message = $request->input('message');
            $report->save();
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function statistics(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $news = News::find($id);

        if($news) {
            $news_statistic = NewsStatistic::firstOrNew(['news_id' => $news->id]);

            if($request->input('type') == 'views' || $request->input('type') == 'downloads') {
                if($request->input('type') == 'views') {
                    $news_statistic->media_views = $news_statistic->media_views + 1;
                }
                if($request->input('type') == 'downloads') {
                    $news_statistic->media_downloads = $news_statistic->media_downloads + 1;
                }
                $news_statistic->save();

                return [
                    'status' => true,
                    'data' => [
                        'id' => $news->id,
                        'media_views' => $news_statistic->media_views == null? 0: $news_statistic->media_views,
                        'media_downloads' => $news_statistic->media_downloads == null? 0: $news_statistic->media_downloads
                    ]
                ];
            }

            return [
                'status' => false,
                'message' => 'wrong type field'
            ];
        }

        return [
            'status' => false,
            'message' => 'news not found'
        ];
    }

    public function testNotification(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'included_segment' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $news = News::find($request->query('id'));

        if($news && ($request->query('included_segment') !== 'All'))  {
            $data = [
                'id' => $news->id,
                'title' => $news->headline,
                'content' => $news->article,
                'additional_data' => [
                    'type' => 'news',
                    'id' => $news->id
                ]
            ];

            $news->load('thumbs');

            $thumb = $news->thumbs->first();
            if($thumb) {
                $data['big_picture'] = $thumb->full_path;
            }

            if($request->input('android_channel_id') !== null) {
                $data['android_channel_id'] = $request->input('android_channel_id');
            }

            if($request->input('send_after') !== null) {
                $data['send_after'] = $request->input('send_after');
            }

            if($request->query('included_segment') == 'Android Test' || $request->query('included_segment') == 'iOS Test') {
                $included_segments = [$request->query('included_segment')];
                $response = $this->onesignalNotification($data, $included_segments);

                return $response;
            }

            return [
                'status' => false,
                'message' => 'Wrong Segment'
            ];
        }

        return [
            'status' => false
        ];
    }

    public function notificationClick(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];
        
        try {
            NewsNotification::where('news_id', $request->input('news_id'))->where('platform', 'Android')->update([
                'clickes'=> \DB::raw('clickes+1')
            ]);
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function newsDeletedUpdatedIds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_ids' => 'required',
            'last_request_ts' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = [
            'status' => true,
            'server_time' => time() * 1000
        ];
        
        try {
            $device_data = Redis::get('device_data:'. $request->input('device_id'));

            if($device_data !== null) {
                $device_data = json_decode($device_data);

                $device = $device_data->device;

                $news_ids = json_decode($request->input('news_ids'), true);

                $last_request_ts = ($request->input('last_request_ts') / 1000);
                $last_request_ts = Carbon::createFromTimestamp($last_request_ts)->toDateTimeString();

                $deleted_ids = News::onlyTrashed()->whereIn('id', $news_ids)->where('deleted_at', '>', $last_request_ts)->get()->pluck('id')->toArray();
                $updated_ids = News::whereIn('id', $news_ids)->where('updated_at', '>', $last_request_ts)->get()->pluck('id')->toArray();

                $elastic_search_params = '{
                    "index":"short_news",
                    "body":{
                        "query":{
                            "bool":{
                                "filter":{
                                    "terms":{
                                        "id":'. json_encode($updated_ids) .'
                                    }
                                }
                            }
                        },
                        "sort":{
                            "datetime":{
                                "order":"desc"
                            }
                        }
                    }
                }';

                $elastic_search_params = json_decode($elastic_search_params, true);

                $raw_news = \Elasticsearch::search($elastic_search_params);
                $news = [];

                foreach ($raw_news['hits']['hits'] as $key => $raw_news_single) {
                    $is_sensitive_viewed = 0;
                    if(in_array($device->id, $raw_news_single['_source']['sensitivities'])) $is_sensitive_viewed = 1;
                    $raw_news_single['_source']['is_sensitive_viewed'] = $is_sensitive_viewed;

                    $raw_news_single['_source']['is_bookmarked'] = 0;
                    if(in_array($device->id, $raw_news_single['_source']['bookmarks'])) $raw_news_single['_source']['is_bookmarked'] = 1;

                    unset($raw_news_single['_source']['sensitivities']);
                    unset($raw_news_single['_source']['bookmarks']);

                    if(isset($raw_news_single['_source']['poll']) && $raw_news_single['_source']['poll'] !== null) {
                        $raw_news_single['_source']['poll'] = $this->pollResponce($device, $raw_news_single['_source']['poll']); 
                    }

                    $news[] = $raw_news_single['_source'];
                }

                $data['updated_ids'] = $news;

                $data['deleted_ids'] = $deleted_ids;
            } else {
                $data['updated_ids'] = [];

                $data['deleted_ids'] = [];
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
