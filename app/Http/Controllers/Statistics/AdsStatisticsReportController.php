<?php

namespace App\Http\Controllers\Statistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Campaign;
use App\CampaignDevice;
use App\CampaignDeviceImpression;

class AdsStatisticsReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query_parameters = $request->query();

        $campaigns = Campaign::with('advertiser')->get();

        $campaign_banner_clicks = CampaignDevice::orderBy('date', 'DESC')
                            ->select(array(
                                \DB::raw('Date(created_at) as date'),
                                \DB::raw('COUNT(*) as "clicks"')
                            ))
                            ->where('created_at', '!=', '0000-00-00');
        if(isset($query_parameters['campaign'])) {
            $campaign_banner_clicks = $campaign_banner_clicks->where('campaign_id', $query_parameters['campaign']);
        }
        $campaign_banner_clicks = $campaign_banner_clicks->groupBy('date')
                            ->get()
                            ->keyBy('date')
                            ->toArray();

        $campaign_banner_impressions = CampaignDeviceImpression::orderBy('impression_date', 'DESC')
                            ->select(array(
                                \DB::raw('impression_date as date'),
                                \DB::raw('SUM(impressions) as "impressions"')
                            ))
                            ->where('impression_date', '!=', '0000-00-00');
        if(isset($query_parameters['campaign'])) {
            $campaign_banner_impressions = $campaign_banner_impressions->where('campaign_id', $query_parameters['campaign']);
        }
        $campaign_banner_impressions = $campaign_banner_impressions->groupBy('date')
                            ->get()
                            ->keyBy('date')
                            ->toArray();             

        $ads_statistics_reports = [];

        if(count($campaign_banner_clicks) || count($campaign_banner_impressions)) {
            $start_date = date("Y-m-d");
            $end_date = array_key_last($campaign_banner_impressions);

            while (strtotime($start_date) >= strtotime($end_date)) {
                $ads_statistics_reports[$start_date] = [
                    'clicks' => 0,
                    'impressions' => 0,
                ];

                if(isset($campaign_banner_clicks[$start_date])) $ads_statistics_reports[$start_date]['clicks'] = (int) $campaign_banner_clicks[$start_date]['clicks'];
                if(isset($campaign_banner_impressions[$start_date])) $ads_statistics_reports[$start_date]['impressions'] = (int) $campaign_banner_impressions[$start_date]['impressions'];
            
                $start_date = date("Y-m-d", strtotime("-1 day", strtotime($start_date)));
            }
        }        

        return view('pages.ads_statistics_reports.index', compact('query_parameters', 'ads_statistics_reports', 'campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
