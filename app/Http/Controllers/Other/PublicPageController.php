<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\PublicPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PublicPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $public_pages = PublicPage::sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.public.index', compact('public_pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.public.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255', 'unique:public_pages'],
            'content' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('public.pages.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $public_page = new PublicPage();
            $public_page->title = $request->input('title');
            $public_page->slug = \Str::slug($public_page->title);
            $public_page->content = $request->input('content');
            $public_page->save();

            DB::commit();

            \Toastr::success('Public Page Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating public page', 'Error', []);
        }

        return redirect()->route('public.pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $public_page = PublicPage::where('slug', $slug)->first();

        if($public_page) {
            return $public_page->content;
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $public_page = PublicPage::find($id);

        if($public_page) {
            return view('pages.public.create', compact('public_page'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $public_page = PublicPage::find($id);

        if($public_page) {
            $validator = Validator::make($request->all(), [
                'title' => ['required', 'string', 'max:255', 'unique:public_pages,title,' . $id],
                'content' => ['required'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('public.pages.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $public_page->title = $request->input('title');
                $public_page->content = $request->input('content');
                $public_page->update();

                DB::commit();

                \Toastr::success('Public Page Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occurred during updating public page', 'Error', []);
            }

            return redirect()->route('public.pages.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $public_page = PublicPage::find($id);

        if($public_page) {
            $public_page->delete();

            \Toastr::success('Public Page Deleted successfully', 'Success', []);

            return redirect()->route('public.pages.index');
        }

        abort(404);
    }
}
