<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function edit() {
        $setting = Setting::first();

        return view('pages.settings.edit', compact('setting'));
    }

    public function update(Request $request) {
        try {
            DB::beginTransaction();

            $setting = Setting::firstOrNew(['id' => 1]);
            $setting->contact_email = $request->input('contact_email');
            $setting->android_share_app_link = $request->input('android_share_app_link');
            $setting->android_share_description = $request->input('android_share_description');
            $setting->privacy_policy_page_link = $request->input('privacy_policy_page_link');
            $setting->terms_of_use_page_link = $request->input('terms_of_use_page_link');
            $setting->instagram_page_link = $request->input('instagram_page_link');
            $setting->ads_type = $request->input('ads_type');
            $setting->save();

            $public_storage_path = 'app/public/';
            $path = 'settings/';
            $app_path = storage_path($public_storage_path . $path);

            if (!file_exists($app_path)) {
                \File::makeDirectory($app_path, 0777, true);
            }

            if($request->file('news_share_bottom_image') !== null) {
                $old_news_share_bottom_image = $setting->news_share_bottom_image;

                $news_share_bottom_image = $request->file('news_share_bottom_image');
                $news_share_bottom_image_extension = $news_share_bottom_image->getClientOriginalExtension();

                $news_share_bottom_image_filename = \Str::random(16) . '.' . $news_share_bottom_image_extension;

                $news_share_bottom_image->move($app_path, $news_share_bottom_image_filename);
            
                $setting->news_share_bottom_image = $news_share_bottom_image_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_news_share_bottom_image);
            }

            if($request->file('app_share_image') !== null) {
                $old_app_share_image = $setting->app_share_image;

                $app_share_image = $request->file('app_share_image');
                $app_share_image_extension = $app_share_image->getClientOriginalExtension();

                $app_share_image_filename = \Str::random(16) . '.' . $app_share_image_extension;

                $app_share_image->move($app_path, $app_share_image_filename);
            
                $setting->app_share_image = $app_share_image_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_app_share_image);
            }

            DB::commit();

            \Toastr::success('Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during updating settings', 'Error', []);
        }

        return redirect()->route('settings.edit');
    }
}
