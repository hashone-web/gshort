<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\ReportType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

class ReportTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report_types = ReportType::sortable()
            ->paginate(\Config::get('constants.pagination_size'));

        return view('pages.report_types.index', compact('report_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.report_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:report_types'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('report.types.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $report_type = new ReportType();
            $report_type->name = $request->input('name');
            $report_type->gujarati_name = $request->input('gujarati_name');
            $report_type->save();

            DB::commit();

            \Toastr::success('Report Type Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            dd($e);

            \Toastr::error('Error occured during creating report type', 'Error', []);
        }

        return redirect()->route('report.types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report_type = ReportType::find($id);

        if($report_type) {
            return view('pages.report_types.create', compact('report_type'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report_type = ReportType::find($id);

        if($report_type) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:report_types,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('cities.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $report_type->name = $request->input('name');
                $report_type->gujarati_name = $request->input('gujarati_name');
                $report_type->update();

                DB::commit();

                \Toastr::success('Report Type Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating report type', 'Error', []);
            }

            return redirect()->route('report.types.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report_type = ReportType::find($id);

        if($report_type) {
            $report_type->delete();

            \Toastr::success('Report Type Deleted successfully', 'Success', []);

            return redirect()->route('report.types.index');
        }

        abort(404);
    }
}
