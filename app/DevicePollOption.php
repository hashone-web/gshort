<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevicePollOption extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id', 'poll_option_id'
    ];
}
