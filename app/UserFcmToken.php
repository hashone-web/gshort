<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFcmToken extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'token'
    ];
}
