<?php

return [
    "project" => [
        'title' => 'Short News',
        'description' => 'Short News',
        'author' => 'Short News'
    ],
    "pagination_size" => 20,
    "thumb_size" => '515px',
    "notification_types" => [
        'Silent' => 3,
        'Wake' => 2,
        'Vibrate & Wake' => 1,
        'Sound & Wake' => 0,
    ],
];
