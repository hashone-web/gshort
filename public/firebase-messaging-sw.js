self.addEventListener('notificationclick', function (event) {
	if (!event.action) {
		self.clients.openWindow(event.notification.data.click_action, '_blank')
		event.notification.close();
		return;
	} else {
		event.notification.close();
	}
});

importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js');

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBdMfk3XvJtvaXsTTGlbiePw9-3HGTYGAg",
    projectId: "cityplus-web",
    messagingSenderId: "56161085787",
    appId: "1:56161085787:web:32115ea78a1a0e3bc8127e",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    var notification_data = JSON.parse(payload.data.notification);

    return self.registration.showNotification(notification_data.title, notification_data);
});