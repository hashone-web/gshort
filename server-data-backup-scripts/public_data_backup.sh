#!/bin/bash

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

/usr/bin/backblaze-b2 sync --delete "/var/www/gshort.in/storage/app/public" "b2://gshort-storage-backup"
