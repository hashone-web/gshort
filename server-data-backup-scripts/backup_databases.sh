#!/bin/bash

OUTPUT="/var/www/databases-backup"

databases=`mysql --defaults-extra-file=/root/.my.cnf -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`

FOL=$OUTPUT

echo $FOL

mkdir -p $FOL

for db in $databases; do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != "sys" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != "mysql" ]] && [[ "$db" != _* ]] ; then
        echo "Dumping database: $db"
        FILE=$FOL"/"$db.sql
        mysqldump --defaults-extra-file=/root/.my.cnf --databases $db > $FILE
        gzip -9 -f $FILE
    fi
done

/usr/bin/backblaze-b2 sync --keepDays 7 --replaceNewer $OUTPUT "b2://gshort-databases-backup"
